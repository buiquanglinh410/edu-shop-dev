<?php
namespace App\Helper;

class Common {

    public static function getTotalLesson($chapters){
        $total = 0;
        foreach($chapters as $k => $chapter){
            $total += count($chapter->lessons);
        }
        return $total;
    }
    public static function showStatusOrder($status){
        $config_status = config("edushop.order_status");
        $status = isset($config_status[$status]) ? $config_status[$status] : $config_status[0];
        $html = '<span>'.$status.'</span>';
        return $html;
    }
    public static function showPaymentMethod($method){
        $config_status = config("edushop.payment_method");
        $status = isset($config_status[$method]) ? $config_status[$method] : $config_status[0];
        $html = '<span>'.$status.'</span>';
        return $html;
    }
    public static function showTypeProductName($type){
        $config_status = config("edushop.type_product");
        $status = isset($config_status[$type]) ? $config_status[$type] : $config_status['product'];
        $html = '<span>'.$status.'</span>';
        return $html;
    }
    public static function getShortNameController($controller){
        $controller = preg_replace("#Controller#", "", $controller);
        return strtolower($controller);
    }
    public static function createCheckBox($items, $name){
        $html = '';
        foreach($items as $k => $item){
            $html .= '<div class="checkbox">
                               <label><input name="'.$name.'" class="minimal" type="checkbox" value="'.$k.'"> '.$item.'</label>
                      </div>';
        }
        return $html;
    }
    public static function showThumb($folderUpload, $fileName, $type = ""){
        if($type == ""){
            $path = public_path() . "/images/" . $folderUpload . "/" . $fileName;
            if(!empty($fileName) && file_exists($path)){
                return asset("/images/" . $folderUpload . "/" . $fileName);
            }else{
                return asset("/images/default.jpg");
            }
        }else{
            $path = public_path() . "/images/" . $folderUpload . "/" . $type . "/" . $fileName;
            if(!empty($fileName)  && file_exists($path)){
                return asset("/images/" . $folderUpload .  "/" . $type . "/" . $fileName);
            }else{
                return asset("/images/default.jpg");
            }
        }
    }


}


?>
