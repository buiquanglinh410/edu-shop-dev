<?php

namespace App\Http\Controllers\Admin;

use App\blog_tags;
use App\Helper\Common;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\blog_posts as MainModel;
use Illuminate\Support\Facades\Hash;
use Session;

class Blog_postsController extends AdminController
{
    protected $pathView = "admin.core.";
    protected $config = [
        'pagination' => 10,
        'resizeImage' => [
            'thumb' => ['width' => 100],
            'standard' => ['width' => 500]
        ]
    ];
    protected $listFields = [
        [ 'name' => 'id', 'label' => 'Id', 'type' => 'text'],
        [ 'name' => 'name', 'label' => 'Name', 'type' => 'text'],
        [ 'name' => 'description', 'label' => 'Description', 'type' => 'text'],
        [ 'name' => 'picture', 'label' => 'Picture', 'type' => 'thumb'],
        [ 'name' => 'status', 'label' => 'Status', 'type' => 'status'],
        [ 'name' => 'created_at', 'label' => 'Created At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
        [ 'name' => 'updated_at', 'label' => 'Updated At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
    ];
    protected $formFields = [
        'general_tab' => [
            'label_tab' => 'General',
            'items' => [
                [ 'label' => 'Name' ,'name' => 'name', 'type' => 'text'],
                [ 'label' => 'Description' ,'name' => 'description', 'type' => 'text'],
                [ 'label' => 'Content' ,'name' => 'content', 'type' => 'ckeditor'],
                [ 'label' => 'Danh mục' ,'name' => 'category_id', 'type' => 'select', 'data_source' => \App\blog_categories::class, 'foreign_key' => 'category_id' ],
                [ 'label' => 'Tags' ,'name' => 'tag_id', 'type' => 'tag','data_source' => \App\blog_tags::class,  'foreign_key' => 'tags'],
//                [ 'label' => 'Vị trí hiển thị' ,'name' => 'location', 'type' => 'select','data_source' => [
//                        'post_home' => 'Bài viết trang chủ',
//                    ]
//                ],
                [ 'label' => 'Thumbnail' ,'name' => 'picture', 'type' => 'file'],
                [ 'label' => 'Status' ,'name' => 'status', 'type' => 'status'],
//                [ 'label' => 'Images' ,'name' => 'gallery', 'type' => 'file_multi'],
            ]
        ],
        'seo_tab' => [
            'label_tab' => 'Meta',
            'items' => [
                [ 'label' => 'Slug' ,'name' => 'slug', 'type' => 'slug'],
                [ 'label' => 'Meta title' ,'name' => 'meta_title', 'type' => 'text'],
                [ 'label' => 'Meta description' ,'name' => 'meta_description', 'type' => 'textarea'],
                [ 'label' => 'Meta keywords' ,'name' => 'meta_keywords', 'type' => 'text'],
            ]
        ]
    ];
    protected $searchList = [
        'all' => 'Search By All',
        'id' => 'Search By Id',
        'name' => 'Search By Name'
    ];
    protected $notAcceptedCrud = [  '_token'];
    public function __construct(){
        $controller = (new \ReflectionClass($this))->getShortName();
        $shortController = Common::getShortNameController($controller);
        $this->controllerName = $shortController;
        $this->folderUpload = $shortController;
        view()->share("controller", $shortController);
        view()->share("folderUpload", $this->folderUpload);
        view()->share("pathView", $this->pathView);
        view()->share("formFields", $this->formFields);
        view()->share("listFields", $this->listFields);
        view()->share("searchList", $this->searchList);
        view()->share("controllerName", $this->controllerName);
        $this->model = new MainModel();
    }
    // option validate Store
    protected function validateStore(Request $request){
        $request->validate([
            'name' => 'required'
        ],[
            'required' => ':attribute không được rỗng'
        ],[
            'name' => 'Tên'
        ]);
    }
    // option validate Update
    protected function validateUpdate(Request $request, $id = ""){

    }
//    public function edit($id)
//    {
//        $item = $this->model->find($id);
//        return view($this->pathView . 'form')->with('item', $item);
//    }
//    public function store(Request $request)
//    {
//        // validate
//        $data = [
//          'name' => $request->name,
//        ];
//        // upload hinh
//        $data['picture'] = $this->uploadThumb($request->picture);
//        MainModel::create($data);
//        Session::flash('success', 'Bạn đã thêm mới thành công');
//        return redirect()->route('admin.' . $this->controllerName . ".index" );
//    }
}
