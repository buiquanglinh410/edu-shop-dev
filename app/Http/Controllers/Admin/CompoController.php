<?php

namespace App\Http\Controllers\Admin;

use App\Compo;
use App\Helper\Common;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Compo as MainModel;
use Illuminate\Support\Facades\Hash;
use Session;

class CompoController extends Controller
{
    public function __construct()
    {
        if(isset($_GET['tab_current'])){
            Session::put('tab_current', $_GET['tab_current']);
        }
    }

    public function store(Request $request){
        $compo = Compo::find($request->compo_id);
        if(!$compo){
            $compo = new Compo();
        }
        $compo->name = $request->name;
        $compo->price = $request->price;
        $compo->course_id = $request->product_id;
        $compo->save();
        return redirect()->back();
    }
    public function delete($id){
        Compo::find($id)->delete();
        return redirect()->back();
    }
    public function addToCompo(Request $request){
        $compo = Compo::find($request->compo_id);
        $compo->products()->attach($request->product_id, [ 'quantity' => $request->quantity ] );
        return redirect()->back();
    }
    public function deleteItemInCompo(Request $request){
        $compo = Compo::find($request->compo_id);
        $compo->products()->detach($request->product_id);
        return redirect()->back();
    }

}
