<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Common;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Product_products;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Course_course as MainModel;
use Illuminate\Support\Facades\Hash;
use Session;

class Course_coursesController extends AdminController
{
    protected $pathView = "admin.page.course.";
    protected $config = [
        'pagination' => 10,
        'resizeImage' => [
            'thumb' => ['width' => 100],
            'standard' => ['width' => 500]
        ]
    ];
    protected $listFields = [
        [ 'name' => 'id', 'label' => 'Id', 'type' => 'text'],
        [ 'name' => 'name', 'label' => 'Name', 'type' => 'text'],
//        [ 'name' => 'short_description', 'label' => 'Short Description', 'type' => 'text'],
        [ 'name' => 'price_base', 'label' => 'Price Base', 'type' => 'text'],
        [ 'name' => 'picture', 'label' => 'Picture', 'type' => 'thumb'],
        [ 'name' => 'status', 'label' => 'Status', 'type' => 'status'],
        [ 'name' => 'created_at', 'label' => 'Created At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
        [ 'name' => 'updated_at', 'label' => 'Updated At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
    ];
    protected $formFields = [

    ];
    protected $searchList = [
        'all' => 'Search By All',
        'id' => 'Search By Id',
        'name' => 'Search By Name'
    ];
    protected $notAcceptedCrud = [  '_token'];
    public function __construct(){
        $controller = (new \ReflectionClass($this))->getShortName();
        $shortController = Common::getShortNameController($controller);
        $this->controllerName = $shortController;
        $this->folderUpload = $shortController;
        view()->share("controller", $shortController);
        view()->share("folderUpload", $this->folderUpload);
        view()->share("pathView", $this->pathView);
        view()->share("formFields", $this->formFields);
        view()->share("listFields", $this->listFields);
        view()->share("searchList", $this->searchList);
        view()->share("controllerName", $this->controllerName);
        if(isset($_GET['tab_current'])){
            Session::put('tab_current', $_GET['tab_current']);
        }
        $this->model = new MainModel();
    }
    public function edit($id)
    {
        $item = $this->model->find($id);
        $data['products'] = Product_products::orderBy('name','desc')->paginate(10);
        $data['item'] =  $item;

        return view($this->pathView . 'form')->with($data);
    }
    public function store(Request $request)
    {
        $this->validateStore($request);
        $course = new MainModel();
        $course->name = $request->name;
        $course->price_base = $request->price_base;
//        $course->price_final = $request->price_final;
        $course->short_description = $request->short_description;
        $course->category_id = $request->category_id;
        $course->slug = $request->slug;
        $course->content = $request->content;
        $course->result = $request->result;
        $course->hoclieu = $request->hoclieu;
        $course->giangvien = $request->giangvien;
        $course->meta_title = $request->meta_title;
        $course->meta_description = $request->meta_description;
        $course->meta_keywords = $request->meta_keywords;
        if($request->picture){
            $picture_name = $this->uploadThumb($request->picture);
            $course->picture = $picture_name;
        }
        if($request->gallery){
            $arrNameGallery = [];
            foreach($request->gallery as $k => $filePicture){
                $arrNameGallery[] = $this->uploadThumb($filePicture);
            }
            $course->gallery = json_encode($arrNameGallery);
        }
        $course->save();
        Session::flash('success', 'Bạn đã thêm mới thành công');
        return redirect()->route('admin.' . $this->controllerName . ".index" );
    }
    public function update(Request $request, $id)
    {
        $course = MainModel::findOrFail($id);
        $course->name = $request->name;
        $course->price_base = $request->price_base;
//        $course->price_final = $request->price_final;
        $course->short_description = $request->short_description;
        $course->category_id = $request->category_id;
        $course->slug = $request->slug;
        $course->meta_title = $request->meta_title;
        $course->content = $request->content;
        $course->result = $request->result;
        $course->hoclieu = $request->hoclieu;
        $course->giangvien = $request->giangvien;
        $course->meta_description = $request->meta_description;
        $course->meta_keywords = $request->meta_keywords;
        if($request->picture){
            // delete thumb
            if(!empty(trim(($course->picture)))){
                $this->deleteThumb($course->picture);
            }
            // upload thumb
            $picture_name = $this->uploadThumb($request->picture);
            $course->picture = $picture_name;
        }
        if($request->gallery){
            $galleryCurrent = json_decode($course->gallery,true);
            foreach($galleryCurrent as $k => $pictureName){
                $this->deleteThumb($pictureName);
            }
            $arrNameGallery = [];
            foreach($request->gallery as $k => $filePicture){
                $arrNameGallery[] = $this->uploadThumb($filePicture);
            }
            $course->gallery = json_encode($arrNameGallery);
        }
        $course->save();
        Session::flash('success', 'Bạn đã cập nhật thành công');
        return redirect()->route('admin.' . $this->controllerName . ".index" );
    }
    // option validate Store
    protected function validateStore(Request $request){
        $request->validate([
            'name' => 'required|min:3|max:50',
            'short_description' => "required|min:3|max:255",
            'content' => "required|min:3",
            'price_base' => "required|min:3",
//            'price_final' => "required|min:3",
            'category_id' => 'exists:course_categories,id',
            'picture' => "required",
            'gallery' => "required",
            'slug' => 'required',
            'result' => 'required|min:3',
            'hoclieu' => 'required|min:3',
            'giangvien' => 'required|min:3',
        ],[
            'required' => ":attribute không được để trống",
            'min' => ":attribute ít nhất :min ký tự",
            'max' => ":attribute vượt quá :max ký tự",
            'exists' => ":attribute phải được chọn",
        ],[
            'name' => 'Tên',
            'short_description' => 'Mô tả ngắn',
            'content' => 'Thông tin chung',
            'price_base' => 'Giá cơ bản',
//            'price_final' => 'Giá chính thức',
            'picture' => 'Hình ảnh',
            'gallery' => 'Danh sách hình ảnh',
            'type' => 'Loại sản phẩm',
            'category_id' => 'Danh mục',
            'result' => 'Kết quả',
            'hoclieu' => 'Học liệu',
            'giangvien' => 'Giảng viên',
        ]);
    }
    // option validate Update
    protected function validateUpdate(Request $request, $id = ""){
        $request->validate([
            'name' => 'required|min:3|max:50',
            'short_description' => "required|min:3|max:100",
            'content' => "required|min:3|max:100",
            'price_base' => "required|min:3",
//            'price_final' => "required|min:3",
            'picture' => "required",
            'gallery' => "required",
            'type' => "required",
            'category_id' => 'exists:course_categories,id',
            'slug' => 'required',
            'result' => 'required|min:3',
            'hoclieu' => 'required|min:3',
            'giangvien' => 'required|min:3',
        ],[
            'required' => ":attribute không được để trống",
            'min' => ":attribute ít nhất :min ký tự",
            'max' => ":attribute vượt quá :max ký tự",
            'exists' => ":attribute phải được chọn",
        ],[
            'name' => 'Tên',
            'short_description' => 'Mô tả ngắn',
            'content' => 'Thông tin chung',
            'price_base' => 'Giá cơ bản',
//            'price_final' => 'Giá chính thức',
            'picture' => 'Hình ảnh',
            'gallery' => 'Danh sách hình ảnh',
            'type' => 'Loại sản phẩm',
            'category_id' => 'Danh mục',
            'result' => 'Kết quả',
            'hoclieu' => 'Học liệu',
            'giangvien' => 'Giảng viên',
        ]);
    }

}
