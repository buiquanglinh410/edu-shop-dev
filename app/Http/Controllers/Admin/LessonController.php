<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Common;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Lesson;
use Illuminate\Http\Request;
use App\Lesson as MainModel;
use Illuminate\Support\Facades\Hash;
use Session;
use Vimeo\Laravel\Facades\Vimeo;
use Illuminate\Support\Str;
use File;
use Illuminate\Support\Facades\Http;

class LessonController extends AdminController
{
    protected $pathView = "admin.page.lesson.";
    protected $config = [
        'pagination' => 10,
        'resizeImage' => [
            'thumb' => ['width' => 100],
            'standard' => ['width' => 300]
        ]
    ];
    protected $listFields = [
        [ 'name' => 'id', 'label' => 'Id', 'type' => 'text'],
        [ 'name' => 'name', 'label' => 'Name', 'type' => 'text'],
        [ 'name' => 'status', 'label' => 'Status', 'type' => 'status'],
        [ 'name' => 'created_at', 'label' => 'Created At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
        [ 'name' => 'updated_at', 'label' => 'Updated At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
    ];
    protected $formFields = [
        'general_tab' => [
            'label_tab' => 'General',
            'items' => [
                [ 'label' => 'Name' ,'name' => 'name', 'type' => 'text'],
                [ 'label' => 'Thuộc chương' ,'name' => 'chapter_id', 'type' => 'select', 'data_source' => \App\Chapter::class, 'foreign_key' => 'chapter_id' ],
                [ 'label' => 'Status' ,'name' => 'status', 'type' => 'status'],
            ]
        ]
    ];
    protected $searchList = [
        'all' => 'Search By All',
        'id' => 'Search By Id',
        'name' => 'Search By Name'
    ];
    protected $notAcceptedCrud = [  '_token'];
    public function __construct(){
        $controller = (new \ReflectionClass($this))->getShortName();
        $shortController = Common::getShortNameController($controller);
        $this->controllerName = $shortController;
        $this->folderUpload = $shortController;
        view()->share("controller", $shortController);
        view()->share("folderUpload", $this->folderUpload);
        view()->share("pathView", $this->pathView);
        view()->share("formFields", $this->formFields);
        view()->share("listFields", $this->listFields);
        view()->share("searchList", $this->searchList);
        view()->share("controllerName", $this->controllerName);
        $this->model = new MainModel();
    }

    // option validate Store
    protected function validateStore(Request $request){

    }
    // option validate Update
    protected function validateUpdate(Request $request, $id = ""){

    }
    public function editLesson ($id)
    {
        $item = $this->model->find($id);
        $data['item'] = $item;
        return view($this->pathView . 'form')->with($data);
    }

    public function uploadVideo(Request $request){
        $params = $request->all();
        $nameVideo = [];

        //duration
        $hour = $params['hour'] === null ? 0 : (int)$params['hour'];
        $minute = $params['minute'] === null ? 0 : (int)$params['minute'];
        $seconds = $params['seconds'] === null ? 0 : (int)$params['seconds'];
        $duration = ($hour * 60 * 60) + ($minute * 60) + $seconds;

        //gọi model
        $model = new MainModel();

        //kiểm tra duration
        if($duration != 0){
            // check extension
                if(isset($params['id'])){
                    $model->uploadDuration($params['id'], $duration);
                }
        }

        //chưa bắt đk, kiểm tra dữ liệu
        if(isset($params['video_demo']) && isset($params['video_full'])){
            // check extension
            if($_FILES['video_demo']['type'] === "video/mp4" && $_FILES['video_full']['type'] === "video/mp4"){

                //upload video lên vimeo
                $vimeo_demo = Vimeo::connection('main')->upload($params['video_demo']->getRealPath());
                $vimeo_full = Vimeo::connection('main')->upload($params['video_full']->getRealPath());

                //lay ten video vua upload
                $nameVideo['video_demo'] = explode('/', $vimeo_demo)[2];
                $nameVideo['video_full'] = explode('/', $vimeo_full)[2];

                //upload video vao folder
//                $nameVideo = $this->saveVideo($_FILES);
                if(isset($params['id'])){
//                    $nameVideoDemo = $model->get_item_by_id($params['id'])[0]->video_demo;
//                    $nameVideoFull = $model->get_item_by_id($params['id'])[0]->video_full;

                    // Delete video vimo
//                    $edu_lesson = Lesson::where('id', $params['id'])->first();
//                    //lay ten video trong data
//                    $name_video_demo = $edu_lesson->video_demo;
//                    $name_video_full = $edu_lesson->video_full;
//                    //goi model tim kiem
//                    $video_demo = Vimeo::request('/me/videos', ['per_page' => 10], 'GET')['body']['data'];
//                    // tim kiem sau do delete
//                    foreach ($video_demo as $key => $value){
//                        $uri = $value['uri'];
//                        if(explode('/', $value['uri'])[2] === $name_video_demo){
//                            Vimeo::request($uri, [], 'DELETE');
//                        }
//                        if(explode('/', $value['uri'])[2] === $name_video_full){
//                            Vimeo::request($uri, [], 'DELETE');
//                        }
//                    }
                    // luu ten vao data
                    $model->uploadVideoLesson($params['id'], $nameVideo);

//                    $this->deleteVideo($nameVideoDemo, 'video_demo');
//                    $this->deleteVideo($nameVideoFull, 'video_full');
                }
            }
        }

        //  upload file VTT text track demo
        if(isset($params['textTrackDemo'])){
            $lengTextTrack = count(explode('.', $_FILES['textTrackDemo']['name']));
            $ext = explode('.', $_FILES['textTrackDemo']['name'])[$lengTextTrack - 1];
            if($ext === "VTT"){
                $edu_lesson = Lesson::where('id', $params['id'])->first();
                $name_video_demo = $edu_lesson->video_demo;

                $videos = Vimeo::request('/me/videos', ['per_page' => 10], 'GET')['body']['data'];
                // tim kiem sau do delete
                foreach ($videos as $key => $value){
                    $uri = $value['uri'];
                    if(explode('/', $value['uri'])[2] === $name_video_demo){
                        $textTrackBody = Vimeo::request($value['metadata']['connections']['texttracks']['uri'], ["type"=> "subtitles", "language" => "vi", "name" => "Việt Nam"], 'POST')['body'];
                        $textVTT = "WEBVTT

00:01.000 --> 00:03.000
Hello Đây là Mr. Quang Tèo

00:04.000 --> 00:06.000
Sinh ra trong con phố không quan tâm ai sang nghèo.

00:08.000 --> 00:09.000
Drop drop drop =))";
                        $response = Http::withHeaders([
                            'Authorization' => 'bearer 09aeb6f0793423bfecf65c6b659d384a',
                            'Accept' => 'application/vnd.vimeo.*+json;version=3.4',
                            "Content-Type"=>"text/plain"
                        ])->withBody($textVTT,"text/plain")->put($textTrackBody['link'], $textVTT);
                        $textTrackActive = Vimeo::request($textTrackBody['uri'], ["active" => true], 'PATCH');
                    }
                }
            }
        }
        //text track full
        if(isset($params['textTrackFull'])){
            $lengTextTrack = count(explode('.', $_FILES['textTrackFull']['name']));
            $ext = explode('.', $_FILES['textTrackFull']['name'])[$lengTextTrack - 1];
            if($ext === "VTT") {
                $edu_lesson = Lesson::where('id', $params['id'])->first();
                $name_video_full = $edu_lesson->video_full;

                $videos = Vimeo::request('/me/videos', ['per_page' => 10], 'GET')['body']['data'];
                // tim kiem sau do delete
                foreach ($videos as $key => $value) {
                    $uri = $value['uri'];
                    if (explode('/', $value['uri'])[2] === $name_video_full) {
                        $textTrackBody = Vimeo::request($value['metadata']['connections']['texttracks']['uri'], ["type" => "subtitles", "language" => "vi", "name" => "Việt Nam"], 'POST')['body'];
                        $textVTT = "WEBVTT

00:01.000 --> 00:03.000
Hello Đây là Mr. Quang Tèo

00:04.000 --> 00:06.000
Sinh ra trong con phố không quan tâm ai sang nghèo.

00:08.000 --> 00:09.000
Drop drop drop =))";
                        $response = Http::withHeaders([
                            'Authorization' => 'bearer 09aeb6f0793423bfecf65c6b659d384a',
                            'Accept' => 'application/vnd.vimeo.*+json;version=3.4',
                            "Content-Type" => "text/plain"
                        ])->withBody($textVTT, "text/plain")->put($textTrackBody['link'], $textVTT);
                        $textTrackActive = Vimeo::request($textTrackBody['uri'], ["active" => true], 'PATCH');
                    }
                }
            }
        }

        if(isset($params['image-thumbnail'])){
            //kiểm tra đuôi file image
            $lengPictureThumnailk = count(explode('.', $_FILES['image-thumbnail']['name']));
            $extPictureThumbnail = explode('.', $_FILES['image-thumbnail']['name'])[$lengPictureThumnailk - 1];
            if($extPictureThumbnail === "png" || $extPictureThumbnail === "jpg" || $extPictureThumbnail === "jpeg"){

                $thumnailNewName = $this->uploadThumb($params['image-thumbnail']);

                $image = public_path() . "/images/lesson/" . $thumnailNewName;
                $data = fopen ($image, 'rb');
                $size=filesize ($image);
                $contents= fread ($data, $size);
                fclose ($data);

                if(isset($params['id'])){
                    $nameImageThumb = $model->get_item_by_id($params['id'])[0]->thumbnail;
                    $model->uploadImageLesson($params['id'], $thumnailNewName);
                    $this->deleteThumb($nameImageThumb);
                }

                $edu_lesson = Lesson::where('id', $params['id'])->first();
                $name_video_demo = $edu_lesson->video_demo;
                $name_video_full = $edu_lesson->video_full;

                $videos = Vimeo::request('/me/videos', ['per_page' => 10], 'GET')['body']['data'];
                // tim kiem sau do delete
                foreach ($videos as $key => $value){
                    $uri = $value['uri'];
                    if(explode('/', $value['uri'])[2] === $name_video_demo){
                        $pictureBody = Vimeo::request($value['metadata']['connections']['pictures']['uri'], [], 'POST')['body'];
                        $response = Http::withHeaders([
                            'Authorization' => 'bearer 09aeb6f0793423bfecf65c6b659d384a',
                            'Accept' => 'application/vnd.vimeo.*+json;version=3.4',
                            "Content-Type"=>"text/plain"
                        ])->withBody($contents, 'image/' . $extPictureThumbnail)->put($pictureBody['link'], $contents);
                        $pictureActive = Vimeo::request($pictureBody['uri'], ["active" => true], 'PATCH');
                    }
                    if (explode('/', $value['uri'])[2] === $name_video_full) {
                        $pictureBody = Vimeo::request($value['metadata']['connections']['pictures']['uri'], [], 'POST')['body'];
                        $response = Http::withHeaders([
                            'Authorization' => 'bearer 09aeb6f0793423bfecf65c6b659d384a',
                            'Accept' => 'application/vnd.vimeo.*+json;version=3.4',
                            "Content-Type"=>"text/plain"
                        ])->withBody($contents, 'image/' . $extPictureThumbnail)->put($pictureBody['link'], $contents);
                        $pictureActive = Vimeo::request($pictureBody['uri'], ["active" => true], 'PATCH');
                    }
                }
            }
        }

        return redirect()->route('admin.lesson.editLesson', $params['id']);
    }

    public function saveVideo($file){
            $arrNewNameVideos = [];
            $output_dir_videos = public_path() . "/storage/lesson/";
            //video demo
            $videoDemoName = str_replace(' ','-',strtolower($file['video_demo']['name']));
            $videoDemoExt = substr($videoDemoName, strrpos($videoDemoName, '.'));
            $NewVideoDemoName = Str::random(10) . $videoDemoExt;
            //video full
            $videoFullName = str_replace(' ','-',strtolower($file['video_full']['name']));
            $videoFullExt = substr($videoFullName, strrpos($videoFullName, '.'));
            $NewVideoFullName = Str::random(10) . $videoFullExt;
            if (!file_exists($output_dir_videos)){
                @mkdir($output_dir_videos, 0777);
            }
            move_uploaded_file($file['video_demo']["tmp_name"],$output_dir_videos ."video_demo/" . $NewVideoDemoName);\
            move_uploaded_file($file['video_full']["tmp_name"],$output_dir_videos ."video_full/" . $NewVideoFullName);
            $arrNewNameVideos['video_demo'] = $NewVideoDemoName;
            $arrNewNameVideos['video_full'] = $NewVideoFullName;
            return  $arrNewNameVideos;
    }

    public function deleteVideo($videoName, $type){
        $output_dir_videos = public_path() . "/storage/lesson/" . $type . "/" . $videoName;
        File::delete($output_dir_videos);
    }

}
