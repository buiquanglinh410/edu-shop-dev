<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Common;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product_products as MainModel;
use Illuminate\Support\Facades\Hash;
use Session;

class Product_productsController extends AdminController
{
    protected $pathView = "admin.core.";
    protected $config = [
        'pagination' => 10,
        'resizeImage' => [
            'thumb' => ['width' => 100],
            'standard' => ['width' => 300]
        ]
    ];
    protected $listFields = [
        [ 'name' => 'id', 'label' => 'Id', 'type' => 'text'],
        [ 'name' => 'name', 'label' => 'Name', 'type' => 'text'],
        [ 'name' => 'short_description', 'label' => 'Short Description', 'type' => 'text'],
        [ 'name' => 'price_base', 'label' => 'Price Base', 'type' => 'text'],
        [ 'name' => 'picture', 'label' => 'Picture', 'type' => 'thumb'],
        [ 'name' => 'status', 'label' => 'Status', 'type' => 'status'],
        [ 'name' => 'created_at', 'label' => 'Created At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
        [ 'name' => 'updated_at', 'label' => 'Updated At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
    ];
    protected $formFields = [
        'general_tab' => [
            'label_tab' => 'General',
            'items' => [
                [ 'label' => 'Name' ,'name' => 'name', 'type' => 'text'],
                [ 'label' => 'Description' ,'name' => 'short_description', 'type' => 'text'],
                [ 'label' => 'Content', 'name' => 'content', 'type' => 'ckeditor'],
                [ 'label' => 'Price Base' ,'name' => 'price_base', 'type' => 'text'],
                [ 'label' => 'Price Final' ,'name' => 'price_final', 'type' => 'text'],

                [ 'label' => 'Picture' ,'name' => 'picture', 'type' => 'file'],
                [ 'label' => 'Gallery' ,'name' => 'gallery', 'type' => 'file_multi'],
                [ 'label' => 'Danh mục' ,'name' => 'category_id', 'type' => 'select', 'data_source' => \App\Product_category::class, 'foreign_key' => 'category_id' ],
                [ 'label' => 'Loại sản phẩm', 'name' => 'type', 'type' => 'select', 'data_source' =>
                    [
                        'hot' => 'Hiển thị là sản phẩm hot',
                        'features' => 'Hiển thị là sản phẩm nổi bật'
                    ]
                ],
                [ 'label' => 'Status' ,'name' => 'status', 'type' => 'status'],


            ]
        ],
        'seo_tab' => [
            'label_tab' => 'Meta',
            'items' => [
                [ 'label' => 'Slug' ,'name' => 'slug', 'type' => 'slug'],
                [ 'label' => 'Meta Title' ,'name' => 'meta_title', 'type' => 'text'],
                [ 'label' => 'Meta Description' ,'name' => 'meta_description', 'type' => 'textarea'],
                [ 'label' => 'Meta Keywords' ,'name' => 'meta_keywords', 'type' => 'text'],
            ]
        ]
    ];
    protected $searchList = [
        'all' => 'Search By All',
        'id' => 'Search By Id',
        'name' => 'Search By Name'
    ];
    protected $notAcceptedCrud = [  '_token'];
    public function __construct(){
        $controller = (new \ReflectionClass($this))->getShortName();
        $shortController = Common::getShortNameController($controller);
        $this->controllerName = $shortController;
        $this->folderUpload = $shortController;
        view()->share("controller", $shortController);
        view()->share("folderUpload", $this->folderUpload);
        view()->share("pathView", $this->pathView);
        view()->share("formFields", $this->formFields);
        view()->share("listFields", $this->listFields);
        view()->share("searchList", $this->searchList);
        view()->share("controllerName", $this->controllerName);
        $this->model = new MainModel();
    }
    // option validate Store
    protected function validateStore(Request $request){
        $request->validate([
            'name' => 'required|min:3|max:50',
            'short_description' => "required|min:3|max:255",
            'content' => "required|min:3",
            'price_base' => "required|min:3",
            'price_final' => "required|min:3",
            'picture' => "required",
            'gallery' => "required",
//            'type' => "required",
            'category_id' => "exists:product_categories,id",
            'slug' => 'required'
        ],[
            'required' => ":attribute không được để trống",
            'min' => ":attribute ít nhất :min ký tự",
            'max' => ":attribute vượt quá :max ký tự",
            'exists' => ":attribute phải được chọn",
        ],[
            'name' => 'Tên',
            'short_description' => 'Mô tả ngắn',
            'content' => 'Nội dung',
            'price_base' => 'Giá cơ bản',
            'price_final' => 'Giá chính thức',
            'picture' => 'Hình ảnh',
            'gallery' => 'Danh sách hình ảnh',
//            'type' => 'Loại sản phẩm',
            'category_id' => 'Danh mục',
        ]);
    }
    // option validate Update
    protected function validateUpdate(Request $request, $id = ""){
        $request->validate([
            'name' => 'required|min:3|max:50',
            'short_description' => "required|min:3|max:255",
            'content' => "required|min:3",
            'price_base' => "required|min:3",
            'price_final' => "required|min:3",
            'picture' => "required",
            'gallery' => "required",
//            'type' => "required",
            'category_id' => "exists:product_categories,id",
            'slug' => 'required'
        ],[
            'required' => ":attribute không được để trống",
            'min' => ":attribute ít nhất :min ký tự",
            'max' => ":attribute vượt quá :max ký tự",
            'exists' => ":attribute phải được chọn",
        ],[
            'name' => 'Tên',
            'short_description' => 'Mô tả ngắn',
            'content' => 'Nội dung',
            'price_base' => 'Giá cơ bản',
            'price_final' => 'Giá chính thức',
            'picture' => 'Hình ảnh',
            'gallery' => 'Danh sách hình ảnh',
//            'type' => 'Loại sản phẩm',
            'category_id' => 'Danh mục',
        ]);
    }

}
