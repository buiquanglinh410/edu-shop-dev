<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Common;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Widget as MainModel;
use Illuminate\Support\Facades\Hash;
use Session;

class WidgetController extends AdminController
{
    protected $pathView = "admin.core.";
    protected $config = [
        'pagination' => 10,
        'resizeImage' => [
            'thumb' => ['width' => 100],
            'standard' => ['width' => 300]
        ]
    ];
    protected $listFields = [
        [ 'name' => 'id', 'label' => 'Id', 'type' => 'text'],
        [ 'name' => 'name', 'label' => 'Name', 'type' => 'text'],
//        [ 'name' => 'content', 'label' => 'Content', 'type' => 'text'],
        [ 'name' => 'status', 'label' => 'Status', 'type' => 'status'],
        [ 'name' => 'created_at', 'label' => 'Created At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
        [ 'name' => 'updated_at', 'label' => 'Updated At', 'type' => 'datetime', 'format' => 'd/m/Y' ],
    ];
    protected $formFields = [
        'general_tab' => [
            'label_tab' => 'General',
            'items' => [
                [ 'label' => 'Name' ,'name' => 'name', 'type' => 'text'],
                [ 'label' => 'Content' ,'name' => 'content', 'type' => 'ckeditor'],
                [ 'label' => 'Vị trí hiển thị' ,'name' => 'location', 'type' => 'select','data_source' =>
                    [
                        'text__footer' => 'Hiển thị text khối chân trang',
                        'footer_1' => 'Hiển thị khối chân trang 1',
                        'footer_2' => 'Hiển thị khối chân trang 2',
                        'footer_3' => 'Hiển thị khối chân trang 3',
                        'address' => 'Hiển thị địa chỉ',
                        'phone' => 'Hiển thị số điện thoại',
                        'mail' => 'Hiển thị email',
                    ]
                ],
                [ 'label' => 'Link' ,'name' => 'link', 'type' => 'text'],
                [ 'label' => 'Status' ,'name' => 'status', 'type' => 'status'],
            ]
        ]
    ];
    protected $searchList = [
        'all' => 'Search By All',
        'id' => 'Search By Id',
        'name' => 'Search By Name'
    ];
    protected $notAcceptedCrud = [  '_token'];
    public function __construct(){
        $controller = (new \ReflectionClass($this))->getShortName();
        $shortController = Common::getShortNameController($controller);
        $this->controllerName = $shortController;
        $this->folderUpload = $shortController;
        view()->share("controller", $shortController);
        view()->share("folderUpload", $this->folderUpload);
        view()->share("pathView", $this->pathView);
        view()->share("formFields", $this->formFields);
        view()->share("listFields", $this->listFields);
        view()->share("searchList", $this->searchList);
        view()->share("controllerName", $this->controllerName);
        $this->model = new MainModel();
    }
    // option validate Store
    protected function validateStore(Request $request){
        $request->validate([
            'name' => 'required|min:3|max:50',
            'content' => "required|min:3|max:100",
//            'location' => "required",
            'link' => "required",
        ],[
            'required' => ":attribute không được để trống",
            'min' => ":attribute ít nhất :min ký tự",
            'max' => ":attribute vượt quá :max ký tự"
        ],[
            'name' => 'Tên',
            'content' => 'Nội dung',
//            'location' => "Vị trí hiển thị",
            'link' => "Đường dẫn",
        ]);
    }
    // option validate Update
    protected function validateUpdate(Request $request, $id = ""){
        $request->validate([
            'name' => 'required|min:3|max:50',
            'content' => "required|min:3|max:100",
//            'location' => "required",
            'link' => "required",
        ],[
            'required' => ":attribute không được để trống",
            'min' => ":attribute ít nhất :min ký tự",
            'max' => ":attribute vượt quá :max ký tự"
        ],[
            'name' => 'Tên',
            'content' => 'Nội dung',
//            'location' => "Vị trí hiển thị",
            'link' => "Đường dẫn",
        ]);
    }

}
