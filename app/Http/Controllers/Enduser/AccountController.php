<?php

namespace App\Http\Controllers\Enduser;

use App\Http\Controllers\Controller;
use App\Order;
use App\UserCourse;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Session;
class AccountController extends Controller
{

    public function changeProfile(Request $request){
        $user = Auth::user();
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'birthday' => 'required',
        ],[
            'required' => ':attribute không được rỗng'
        ],[
            'first_name' => 'Họ',
            'last_name' => 'Tên',
            'phone' => 'Số điện thoại',
            'birthday' => 'Sinh nhật'
        ]);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->birthday = implode("-", array_reverse(explode("/", $request->birthday)));


        $user->save();

        Session::flash('success', 'Cập nhật thành công');
        return redirect()->route('account.myProfile');
    }
    public function changePassword(Request $request){

        $user = Auth::user();

        $request->validate([
            'password' => 'required|confirmed',
        ],[
            'required' => ':attribute không được rỗng',
            'confirmed' => ':attribute không trùng khớp'
        ],[
            'password' => 'Mật khẩu',
        ]);

        if (Hash::check($request->old_pass, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            Session::flash('success_pass', 'Cập nhật mật khẩu thành công');
        }else{
            Session::flash('error', 'Mật khẩu không chính xác');
        }
        return redirect()->route('account.myProfile');

    }
    public function myProfile(){

        $user = Auth::user();
        $data['user'] = $user;
        return view(config("edushop.end-user.pathView") . "myProfile")->with($data);

    }

    public function myCoupon(){

        return view(config("edushop.end-user.pathView") . "myCoupon");

    }

    public function myCourses(){
        //gọi model
        $user = Auth::user();
        $users = \App\User::find($user->id);
        $list_user_lesson = $user->courses;
        return view(config("edushop.end-user.pathView") . "myCourses")->with("list_user_lesson", $list_user_lesson);

    }
    public function myOrderDetail($order_id){

        $data['order'] = Order::findOrFail($order_id);

        return view(config("edushop.end-user.pathView") . "myOrderDetail")->with($data);

    }
    public function myOrder(){
        $user = Auth::user();
        $data['orders'] = $user->orders()->orderBy('order.id','desc')->paginate(12);

        return view(config("edushop.end-user.pathView") . "myOrder")->with($data);

    }
    public function address(){

        return view(config("edushop.end-user.pathView") . "address");

    }

    public function addAddress(){

        return view(config("edushop.end-user.pathView") . "addAddress");

    }

}
