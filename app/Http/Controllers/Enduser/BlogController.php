<?php

namespace App\Http\Controllers\Enduser;

use App\blog_posts;
use App\Blog_categories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogController extends Controller
{


    public function newDetail($slug){
        $course = blog_posts::where("status","active")->where('slug', $slug)->first();
        $data['new'] = $course;
        return view(config("edushop.end-user.pathView") . "blogDetail")->with($data);
    }
    public function newList(){
        return view(config("edushop.end-user.pathView") . "blogList");
    }
//    public function courseListInCategory(Request $request, $category_slug){
//        $category = Course_category::where('slug', $category_slug)->first();
//        $courses = $category->courses()->where('course_courses.status','active')->orderBy('course_courses.id','desc')->paginate(15);
//
//        $data['category'] = $category;
//        $data['courses'] = $courses;
//        return view(config("edushop.end-user.pathView") . "courseListInCategory")->with($data);
//    }
//    public function lessionDetail(){
//
//        return view(config("edushop.end-user.pathView") . "lessionDetail");
//
//    }
//


}
