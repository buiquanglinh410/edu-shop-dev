<?php

namespace App\Http\Controllers\Enduser;

use App\Comment;
use App\Course_category;
use App\Course_course;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Session;
use App\Chapter;
class CourseController extends Controller
{


    public function courseDetail(Request $request, $category_slug, $course_slug){

        $course = Course_course::where("status","active")->where('slug', $course_slug)->first();
        $course_tag = Course_category::where('id', $course->category_id)->get();
        $data['course'] = $course;
        $data['comments'] = $course->comments('parent_id', 0)->orderBy('comments.id','desc')->get();
        return view(config("edushop.end-user.pathView") . "courseDetail")->with($data)->with('course_tag', $course_tag);

    }

    public function search(Request $request){
        $params = $request->all();
        $course_category = Course_category::all();
        if(isset($params['sort']) && $params['category']){
            if($params['sort'] != "0" && $params['category'] != 0){
                $sort_by = explode("-", $params['sort']);
                $key_word = $params['key_word'];
                $courses['courses'] = Course_course::where('name', 'LIKE', '%' . $key_word . '%')->where('category_id', $params['category'])->orderBy($sort_by[0], $sort_by[1])->paginate(15);
                return view(config("edushop.end-user.pathView") . "course_search")->with($courses)->with('key_word', $key_word)->with('sort_by', $params['sort'])->with('course_category', $course_category)->with('category_id', $params['category']);
            }
        }
        if(isset($params['sort'])){
            if ($params['sort'] != "0") {
                $sort_by = explode("-", $params['sort']);
                $key_word = $params['key_word'];
                $courses['courses'] = Course_course::where('name', 'LIKE', '%' . $key_word . '%')->orderBy($sort_by[0], $sort_by[1])->paginate(15);
                return view(config("edushop.end-user.pathView") . "course_search")->with($courses)->with('key_word', $key_word)->with('sort_by', $params['sort'])->with('course_category', $course_category);
            }
        }
        if (isset($params['category'])){
            if ($params['category'] != 0){
                 $key_word = $params['key_word'];
                 $courses['courses'] = Course_course::where('name', 'LIKE', '%' . $key_word . '%')->where('category_id', $params['category'])->paginate(15);
                 return view(config("edushop.end-user.pathView") . "course_search")->with($courses)->with('key_word', $key_word)->with('sort_by', $params['sort'])->with('course_category', $course_category)->with('category_id', $params['category']);
            }
        }
        if(isset($params['search']) || $params['search'] == null){
            $key_word = $params['search'];
            $courses['courses'] = Course_course::where('name', 'LIKE', '%' . $key_word . '%')->paginate(15);
            return view(config("edushop.end-user.pathView") . "course_search")->with($courses)->with('key_word', $key_word)->with('course_category', $course_category);
        }
        $key_word = $params['key_word'];
        $courses['courses'] = Course_course::where('name', 'LIKE', '%' . $key_word . '%')->paginate(15);
        return view(config("edushop.end-user.pathView") . "course_search")->with($courses)->with('key_word', $key_word)->with('course_category', $course_category);
    }

    public function courseList(){
        $data['categories'] = Course_category::where('status','active')->orderBy('id','desc')->get();
        return view(config("edushop.end-user.pathView") . "courseList")->with($data);
    }

    public function courseListInCategory(Request $request, $category_slug){
        $category = Course_category::where('slug', $category_slug)->first();
        $courses = $category->courses()->where('course_courses.status','active')->orderBy('course_courses.id','desc')->paginate(15);

        $data['category'] = $category;
        $data['courses'] = $courses;
        return view(config("edushop.end-user.pathView") . "courseListInCategory")->with($data);
    }

    public function lessionDetail(){

        return view(config("edushop.end-user.pathView") . "lessionDetail");

    }

    public function lessionDetailChapter($slug_lesson){
        $list_chapter = [];
        $list_lessons = [];
        $course_id = Course_course::where("status","active")->where('slug', $slug_lesson)->first();
        $course = \App\Course_course::find($course_id->id);
        $chapter = $course->chapters;
        foreach ($chapter as $key => $value){
            $list_chapter[$key] = $value->lessons;
        }
        foreach ($list_chapter as $key => $value){
            foreach ($value as $item => $collection){
                $list_lessons[$collection->id] = $collection;
            }
        }
        return view(config("edushop.end-user.pathView") . "lessionDetail")->with('list_lessons', $list_lessons);
    }

    public function addComment(Request $request){
        if($request->method() == "POST"){
            $user = $request->user();
            $course = Course_course::find($request->course_id);
            $parent = User::find($request->parent_id);
            if($course){
                if($request->parent_id == 0 || $parent ){
                    $comment = new Comment();
                    $comment->body = $request->body;
                    $comment->course_id = $request->course_id;
                    $comment->star = $request->star;
                    $comment->parent_id = $request->parent_id;
                    $comment->user_id = $user->id;
                    $comment->save();
                    Session::flash('success', 'Đã đánh giá thành công');
                }
            }
        }
        return redirect()->back();
    }
    public function likeComment(Request $request){
        $like = $request->like == 1 ? 1 : 0;
        $user = $request->user();
        //$update_result  = $user->likes()->sync($request->comment_id, [ 'like' => $like ],true);
        $result = $user->likes()->where('comment_id', $request->comment_id)->exists();
        if(!$result){
            $user->likes()->attach([ $request->comment_id ],  [ 'like' => $like ] );
        }else{
            $user->likes()->where('comment_id', $request->comment_id)->update([ 'like' => $like  ]);
        }
        return redirect()->back();
    }

    public function courseTagSlug(Request $request, $slug){
        $params = $request->all();
        $course_category = Course_category::where('slug', $slug)->first();
        $slug_id = $course_category->id;
        $category_name = $course_category->name;
        $courses = Course_course::where('category_id', $slug_id)->paginate(15);
        if(isset($params['sort'])){
            if ($params['sort'] != "0") {
                $sort_by = explode("-", $params['sort']);
                $courses = Course_course::where('category_id', $slug_id)->orderBy($sort_by[0], $sort_by[1])->paginate(15);
                return view(config("edushop.end-user.pathView") . "courseTag")->with('courses', $courses)->with('category_name', $category_name)->with('slug', $slug)->with('sort_by', $params['sort']);
            }
        }
        return view(config("edushop.end-user.pathView") . "courseTag")->with('courses', $courses)->with('category_name', $category_name)->with('slug', $slug);
    }
}
