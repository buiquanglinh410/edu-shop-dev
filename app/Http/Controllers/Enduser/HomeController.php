<?php

namespace App\Http\Controllers\Enduser;

use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Province;
use App\District;
class HomeController extends Controller
{
    public function index(){
        return view(config("edushop.end-user.pathView") . "index");
    }
    public function getDistrict(Request $request){
        $id_tinh = $request->id_tinh;
        $province = Province::find($id_tinh);
        $districts = $province->districts;
        $html = '<option value="default">Chọn Quận/Huyện</option>';
        foreach($districts as $k => $district){
            $html .= '<option value="'.$district->id.'">'.$district->_name.'</option>';
        }
        return $html;
    }
    public function getWard(Request $request){
        $id_huyen = $request->id_huyen;
        $district = District::find($id_huyen);
        $wards = $district->wards;
        $html = '<option value="default">Chọn Phường/Xã</option>';
        foreach($wards as $k => $ward){
            $html .= '<option value="'.$ward->id.'">'.$ward->_name.'</option>';
        }
        return $html;
    }

}
