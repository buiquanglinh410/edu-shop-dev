<?php

namespace App\Http\Controllers\Enduser;

use App\Compo;
use App\Course_course;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product_products;
use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Session;
use Illuminate\Support\Str;
use App\OrderDetail;
class OrderController extends Controller
{


    public function cart(){
        $cart = Cart::getContent();
        return view(config("edushop.end-user.pathView") . "cart",compact('cart'));
    }
    public function addCart(Request $request){
        $id = $request->id;
        $type = $request->type ? $request->type : "course";
        if($type == "course"){
            $course = Course_course::find($id);
        }else{
            $course = Product_products::find($id);
        }
        if ($id) {
            $id = $type . "-" . $id;
            $data = [
                'id' => $id,
                'name' => $course->name,
                'price' => !empty($course->price_final) ? $course->price_final : 0,
                'quantity' => 1,
                'attributes' => array(
                    'picture' => $course->picture,
                    'type' => $type
                ),
            ];

            if(!empty($request->compo_id)){
                $compo = Compo::find($request->compo_id);
                if($compo){
                    if($type == "course"){
                        $data['attributes']['compo_id'] =  $request->compo_id;
                    }
                }
            }
            Cart::add($data);
            Session::flash('c artSuccess', 'Thêm sản phẩm vào giỏ hàng thành công !!');
            return redirect()->back();
        }
    }
    public  function deleteCart($id){
         Cart::remove($id);
        Session::flash('cartRemove', 'Xóa giỏ hàng thành công !!');
        return redirect()->back();
    }
    public function updateCart(Request $request,$id){
        Cart::update($id, array('quantity' => array(
            'relative' => false,
            'value' => $request->quantity
        ), ));
        Session::flash('cartUpdate', 'Cập nhật giỏ hàng thành công !!');
        return redirect()->back();
    }
    public function checkout(){
        $user = Auth::user();
        return view(config("edushop.end-user.pathView") . "checkout")->with("user", $user);

    }
   public function postCheckout(Request $request){
        //dd($request->all());
       $request->validate( [
           'name' => 'required',
           'phone' => 'required',
           'address' => 'required',
           'province_id' => 'required|exists:province,id',
           'district_id' => 'required|exists:district,id',
           'ward_id' => 'required|exists:ward,id',
           'payment_method' => 'required|in:cod,bank'
       ],[
           'required' => ':attribute không được rỗng',
           'exists' => ':attribute không hợp lệ',
           'min' => ':attribute phải có ít nhất :min kí tự',
           'max' => ':attribute không vượt quá :max kí tự',
           'in' => ':attribute không hợp lệ',
       ],[
           'name' => 'Họ và tên',
           'phone' => 'Số điện thoại',
           'address' => 'Địa chỉ',
           'province_id' => 'Tỉnh/TP',
           'district_id' => 'Quận/Huyện',
           'ward_id' => 'Phường/xã',
           'payment_method' => 'Phương thức thanh toán'
       ]);

       $order = new Order();
       $order->name = $request->name;
       $order->email = $request->email;
       $order->phone = $request->phone;
       $order->province_id = $request->province_id;
       $order->district_id = $request->district_id;
       $order->ward_id = $request->ward_id;
       $order->address = $request->address;
       $order->pay_method = $request->payment_method;
       $order->user_id = Auth::user()->id;
       $order->save();

       $carts = Cart::getContent();
       foreach($carts as $k => $cart){
           $detail = new OrderDetail();
           $detail->product_name = $cart->name;
           $detail->product_price = (int)$cart->price;
           $detail->quantity = $cart->quantity;
           $productId = explode("-",$cart->id);
           $detail->product_id = $productId[1];
           $detail->type = $cart->attributes->type;
           $detail->order_id = $order->id;
           $detail->save();
       }
      // $this->sendMail($order);

       Session::flash('success', 'Thanh toán thành công !');
       return redirect()->route('order.checkoutSuccess', 'order_id=' . $order->id);
   }
   public function checkoutSuccess(Request $request){
        $user = Auth::user();
        $order_id = $request->order_id;
        $order = $user->orders()->where('order.id', $order_id )->first();
        if(!$order){
            return redirect()->back();
        }
        $data['user'] = $user;
        $data['order'] = $order;
        return view(config("edushop.end-user.pathView") . "checkoutSuccess")->with($data);
   }
   public function sendMail($order){

       Mail::send('enduser.mail.mail_order', [ 'order' => $order ] , function($message) use ($order){
           $message->to($order->email, $order->name)->subject('Đơn hàng thành công');
       });
   }

}
