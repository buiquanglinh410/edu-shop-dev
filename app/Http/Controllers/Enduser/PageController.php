<?php

namespace App\Http\Controllers\Enduser;
use App\Config;
use App\Http\Controllers\Controller;
use App\Page;
use App\QA_questionClient;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
class PageController extends Controller
{
    public function __construct()
    {

        view()->share('config', Config::first());
        $routeName = \Request::route()->getName();
        $page_id = 0;
        switch ($routeName){
            case "SiteTeacher":
                $page_id = 19;
                break;
        }
        $page = Page::find($page_id);
        view()->share('page', $page);
    }
    public function becomeTeacher(){

        return view(config("edushop.end-user.pathView") . "becomeTeacher");

    }
    public function listPage(){

        return view(config("edushop.end-user.pathView"). "listPage");

    }
    public function notificationDetaill(){

        return view(config("edushop.end-user.pathView") . "notificationDetaill");

    }
    public function review(){

        return view(config("edushop.end-user.pathView") . "review");

    }
    public function tutorial(){

        return view(config("edushop.end-user.pathView") . "tutorial");

    }
    public function postquestionClient(Request $request){
         $questionClient = new QA_questionClient();
         $questionClient->name = $request->name;
         $questionClient->users_id = Auth::user()->id;
         $questionClient->save();
         Session::flash('success', 'Gửi câu hỏi thành công !!');
         return redirect()->back();
    }
}
