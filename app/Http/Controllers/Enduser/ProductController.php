<?php

namespace App\Http\Controllers\Enduser;

use App\Http\Controllers\Controller;
use App\Product_category;
use App\Product_tags;
use Illuminate\Http\Request;
use App\Product_products;
class ProductController extends Controller
{

    public function productList(Request $request){

        $products = Product_products::where('status','active');
        // fillter
        if($request->name ){
          $products->where('name', 'like', '%'.$request->name.'%');
        }
        if($request->price){
            $p = explode(',', $request->price);
            $products->whereBetween('price_final',array($p[0],$p[1]));
        }
        // sắp xếp
        $products->orderBy('id','DESC');
        // phân trang
        $products = $products->paginate(24);
        $data['products'] = $products;
        return view(config("edushop.end-user.pathView") . "productList")->with($data);

    }
    public function productListByCategory(Request $request, $slug_category){

        $category = Product_category::where('slug', $slug_category)->where('status','active')->first();
        if(!$category){
            Product_category::where('id', $slug_category)->where('status','active')->first();
        }
        if(!$category){
            return abort(404);
        }

        $products = $category->products()->where('product_products.status','active');
        // fillter

        // sắp xếp
        $products->orderBy('product_products.id','DESC');
        // phân trang
        $products = $products->paginate(24);

        $data['products'] = $products;
        $data['category'] = $category;
        return view(config("edushop.end-user.pathView") . "productListByCategory")->with($data);

    }
    public function productDetail($category_slug, $product_slug){

        $product = Product_products::where('slug', $product_slug)->where('status', 'active')->first();
        $product_tag = Product_tags::where('id', $product->category_id)->get();
        if(!$product){
            abort(404);
        }
        $data['product'] = $product;
        return view(config("edushop.end-user.pathView") . "productDetail")->with($data)->with('product_tag', $product_tag);

    }



}
