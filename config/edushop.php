<?php
return [
    'end-user' => [
        'pathView' => 'enduser.page.'
    ],
    'order_status' => [
        0 => 'Đặt hàng thành công',
        1 => 'Hệ thống tiếp nhận',
        2 => 'Đang lấy hàng',
        3 => 'Đang vận chuyển',
        4 => 'Giao hàng thành công',
        5 => 'Đã hủy'
    ],
    'payment_method' => [
        'cod' => 'COD ( trả tiền khi nhận được hàng)',
        'bank' => 'Chuyển ATM' ,
    ],
    'type_product' => [
        'course' => 'Khóa học',
        'product' => 'Học liệu, học cụ',
    ]
];
