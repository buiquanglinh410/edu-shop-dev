<?php
return [


    'post' => [
        'index' => 'List Post',
        'create' => 'Create Post',
        'edit' => 'Edit Post',
        'destroy' => 'Destroy Post',
    ],
    'user' => [
        'index' => 'List User',
        'create' => 'Create User',
        'edit' => 'Edit User',
        'destroy' => 'Destroy user',
        'changeLang' => 'Change Language'
    ],
    'role' => [
        'index' => 'List Role',
        'create' => 'Create Role',
        'edit' => 'Edit Role',
        'destroy' => 'Destroy Role',
    ],
    'banner' => [
        'index' => 'List Banner',
        'create' => 'Create Banner',
        'edit' => 'Edit Banner',
        'destroy' => 'Destroy Banner',
    ],
    'widget' => [
        'index' => 'List Widget',
        'create' => 'Create Widget',
        'edit' => 'Edit Widget',
        'destroy' => 'Destroy Widget',
    ],
    'discount' => [
        'index' => 'List discount',
        'create' => 'Create discount',
        'edit' => 'Edit discount',
        'destroy' => 'Destroy discount',
    ],
    'page' => [
        'index' => 'List pages',
        'create' => 'Create pages',
        'edit' => 'Edit pages',
        'destroy' => 'Destroy pages',
    ],
    'blog_tags' => [
        'index' => 'List blog tags',
        'create' => 'Create blog tags',
        'edit' => 'Edit blog tags',
        'destroy' => 'Destroy blog tags',
    ],
    'blog_posts' => [
        'index' => 'List blog posts',
        'create' => 'Create blog posts',
        'edit' => 'Edit blog posts',
        'destroy' => 'Destroy blog posts',
    ],
    'address' => [
        'index' => 'List address',
        'create' => 'Create address',
        'edit' => 'Edit address',
        'destroy' => 'Destroy address',
    ],
    'product_tags' => [
        'index' => 'List address',
        'create' => 'Create address',
        'edit' => 'Edit address',
        'destroy' => 'Destroy address',
    ],
    'blog_categories' => [
        'index' => 'List categories',
        'create' => 'Create categories',
        'edit' => 'Edit categories',
        'destroy' => 'Destroy categories',
    ],
    'contact' => [
        'index' => 'List contact',
        'create' => 'Create contact',
        'edit' => 'Edit contact',
        'destroy' => 'Destroy contact',
    ],
    'product_products' => [
        'index' => 'List product_products',
        'create' => 'Create product_products',
        'edit' => 'Edit product_products',
        'destroy' => 'Destroy product_products',
    ],

    'product_category' => [
        'index' => 'List product category',
        'create' => 'Create product category',
        'edit' => 'Edit product category',
        'destroy' => 'Destroy product category',
    ],

    'qa_question' => [
        'index' => 'List Question',
        'create' => 'Create Question',
        'edit' => 'Edit Question',
        'destroy' => 'Destroy Question',
    ],
    'translate' => [
        'index' => 'List translate',
        'edit' => 'Edit translate',
        'destroy' => 'Destroy translate'
    ],
    'qa_answer' => [
        'index' => 'List Answer',
        'create' => 'Create Answer',
        'edit' => 'Edit Answer',
        'destroy' => 'Destroy Answer',
    ],

    'event' => [
        'index' => 'List event',
        'create' => 'Create event',
        'edit' => 'Edit event',
        'destroy' => 'Destroy event',
    ],
    'course_category' => [
        'index' => 'List course_categories',
        'create' => 'Create course_categories',
        'edit' => 'Edit course_categories',
        'destroy' => 'Destroy course_categories',
    ],
    'course_courses' => [
        'index' => 'List course',
        'create' => 'Create course',
        'edit' => 'Edit course',
        'destroy' => 'Destroy course',
    ],
    'notification' => [
        'index' => 'List notification',
        'create' => 'Create notification',
        'edit' => 'Edit notification',
        'destroy' => 'Destroy notification',
    ],
    'chapter' => [
        'index' => 'List chapter',
        'create' => 'Create chapter',
        'edit' => 'Edit chapter',
        'destroy' => 'Destroy chapter',
    ],
    'lesson' => [
        'index' => 'List lesson',
        'create' => 'Create lesson',
        'edit' => 'Edit lesson',
        'destroy' => 'Destroy lesson',
    ],
    'bank' => [
        'index' => 'List bank',
        'create' => 'Create bank',
        'edit' => 'Edit bank',
        'destroy' => 'Destroy bank',
    ],
];


?>
