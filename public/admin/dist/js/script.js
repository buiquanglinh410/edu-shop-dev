function changeSearch(t,k,v){
    var tObj = $(t);
    tObj.parents(".box-search").find(".btn").text(v);
    $("input[name='search_type']").val(k);
}
function searchAction(){
    var type = $("input[name='search_type']").val();
    var value = $("input[name='search_value']").val();
    if(type == "" || value == ""){
        alert('không được rỗng');
        return;
    }
    var pathname	= window.location.pathname;
    console.log(pathname);
    let searchParams= new URLSearchParams(window.location.search);
    var params 			= ['page'];
    let link		= "";
    $.each( params, function( key, value ) {
        if (searchParams.has(value) ) {
            link += value + "=" + searchParams.get(value) + "&"
        }
    });
    window.location.href = pathname + "?" + link  + 'search_type=' + type + '&search_value=' + value;
}
function clearSearchAction(){
    var pathname	= window.location.pathname;
    window.location.href = pathname;
}
function deleteAction(link){
    alertify.confirm('Thông báo', 'Bạn chắc chắn muốn xóa',
        function(){
            window.location.href = link;
        }
        ,
        function(){

        });
}
function deleteMulti(){
    alertify.confirm('Thông báo', 'Bạn chắc chắn muốn xóa',
        function(){
           $("#frmList").submit();
        }
        ,
        function(){

        });
}
$(".picture_multi").change(function() {
    readURLMulty(this);
});
$(".file_picture_one").change(function() {
    readURL(this);
});
function readURLMulty(input) {
    var length = input.files['length'];
    console.log(input);
    $(input).next(".multi_preview_image").html("");
    for (var i = 0; i < length; i++) {

        if (input.files && input.files[i]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                var src = e.target.result;
                // apend img after id
                var img = "<img class='avatar_preview' src=" + src + ">";
                $(input).after(img);
            }

            reader.readAsDataURL(input.files[i]);
        }
    }

}
function readURL(input) {
    var inputObj = $(input);
    console.log(inputObj);
    inputObj.parents(".file_picture").children(".image-upload").remove();
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            var src = e.target.result;
            var img = htmlImg(src);
            inputObj.after(img);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function htmlImg(src) {
    var html = '<div class="image-upload">';
    html += '<img src="' + src + '" class="preview_picture">';
    html += '</div>';
    return html;
}
function submitForm(){
    $('#form_info').submit();
}
function ordering(link){
    alertify.confirm('Thông báo', 'Bạn chắc chắn muốn thay đổi thứ tự',
        function(){
            $("#frmList").attr('action', link);
            $("#frmList").submit();
        }
        ,
        function(){

        });
}
$( document ).ready(function() {

    $("#check-all").on('ifChanged', function(){
        var checked = $(this).prop("checked");
        var check = "";
        if(checked){
            check = "check";
        }else{
            check = "uncheck";
        }
        $("#frmList input[type='checkbox']").iCheck(check);
    });
});
function confirmDelete(link){
    if(confirm('Bạn chắc chắn muốn xóa')){
        window.location.href = link;
    }
}
function showEdit(name, price, id){
    $("#form_add_product input[name='name']").val(name);
    $("#form_add_product input[name='price']").val(price);
    $("#form_add_product").append('<input type="hidden" name="compo_id" value="'+id+'">');
    $("#form_add_product").addClass("in");
}
function resetCreate(){
    $("#form_add_product input[name='name']").val('');
    $("#form_add_product input[name='price']").val('');
    $("#form_add_product").append('<input type="hidden" name="compo_id" value="0">');
}

//show video lesson
function previewVideo(input, inputName, PreVideoID){
    var file = $("input[name='" + inputName + "']").get(0).files[0];
    if(file){
        var reader = new FileReader();
        reader.onload = function(){
            $("#" + PreVideoID).attr("src", reader.result);
            $("#" + PreVideoID).show();
            $("iframe").hide();
        }
        reader.readAsDataURL(file);
    }
}

//show picture lesson
function previewImage(input, inputName, PreImageID){
    var file = $("input[name='" + inputName + "']").get(0).files[0];
    if(file){
        var reader = new FileReader();
        reader.onload = function(){
            $("#" + PreImageID).attr("src", reader.result);
            $("#" + PreImageID).show();
            $(".show_image").hide();
        }
        reader.readAsDataURL(file);
    }
}