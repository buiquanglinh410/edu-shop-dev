@php
    use App\Helper\Form;
    if(!isset($item)){
        $item = [];
        $link = route('admin.'.$controllerName.'.store');
    }else{
        $link = route('admin.'.$controllerName.'.update', [ 'id' => $item['id'] ]  );
    }

@endphp

@extends("admin.layout")

@section('content-header')
    @include("admin.partials.page-header")
@stop
<style>
    img{
        width: 100%;
    }
</style>
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form action="{{ $link  }}" method="POST" enctype="multipart/form-data">
                @csrf
            <div class="box box-primary">
                <div class="box-body">
                    @include("admin.template.error")
                    <div class="edu_tab">
                        @php
                            $keyActive = "general_tab";
                        @endphp
                        <ul class="nav nav-tabs tab_header">
                            <li class="active"><a data-toggle="tab" href="#thongtin">Thông tin chung</a></li>
                            @foreach($formFields as $k => $tab)
                            <li><a data-toggle="tab" href="#{{ $k }}">{{ $tab['label_tab'] }}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            <div id="thongtin" class="page_manager tab-pane fade in active">
                                @php
                                    $content = unserialize($item->content);
                                @endphp
                                @if($item->id == 19)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Name</label>
                                                <input value="{{ @$item->name }}" type="text" class="form-control">
                                            </div>
                                            <section>
                                                <h3>Khối các slider</h3>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="">Tiêu đề</label>
                                                        <input value="{{ @$content['slider' ]['name'] }}" name="content[slider][name]"  type="text" class="form-control">

                                                        <div class="form-group">
                                                            <label for="">Mô tả</label>
                                                            <textarea name="content[slider][description]" class="form-control" cols="30" rows="5">{{ @$content['slider']['description'] }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    @for($stt = 1; $stt < 4; $stt++)
                                                        <div class="col-md-4">
                                                            <h4>Khối thứ {{ $stt }}</h4>
                                                            <div class="form-group file_picture">
                                                                <label for="">Hình ảnh:</label>
                                                                <input name="content[slider_{{ $stt }}][picture]" type="file" class="form-control" id="">
                                                                <p class="image-upload"><img src="{{  \App\Helper\Common::showThumb('page', @$content['slider_' . $stt]['picture'] ) }}" alt=""></p>
                                                            </div>

                                                        </div>
                                                    @endfor
                                                </div>
                                            </section>
                                            <section>
                                                <h3>Khối các dịch vụ</h3>
                                                <div class="row">
                                                    @for($stt = 1; $stt < 4; $stt++)
                                                        <div class="col-md-4">
                                                            <h4>Khối thứ {{ $stt }}</h4>
                                                            <div class="form-group file_picture">
                                                                <label for="">Icon:</label>
                                                                <input name="content[dichvu_khoi_{{ $stt }}][picture]" type="file" class="form-control" id="">
                                                                <p class="image-upload"><img src="{{  \App\Helper\Common::showThumb('page', @$content['dichvu_khoi_' . $stt]['picture'] ) }}" alt=""></p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Tiêu đề</label>
                                                                <input value="{{ @$content['dichvu_khoi_' . $stt ]['name'] }}" name="content[dichvu_khoi_{{ $stt }}][name]"  type="text" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Mô tả</label>
                                                                <textarea name="content[dichvu_khoi_{{ $stt }}][description]" class="form-control" cols="30" rows="5">{{ @$content['dichvu_khoi_' . $stt ]['description'] }}</textarea>
                                                            </div>
                                                        </div>
                                                    @endfor
                                                </div>
                                            </section>
                                            <section>
                                                <h3>Khối "những con số chính"</h3>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group file_picture">
                                                            <label for="">Tiêu đề</label>
                                                            <input value="{{ @$content['conso']['tieude'] }}" name="content[conso][tieude]" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group file_picture">
                                                            <label for="">Background:</label>
                                                            <input name="content[conso][picture]" type="file" class="form-control" id="">
                                                            <p class="image-upload"><img src="{{  \App\Helper\Common::showThumb('page', @$content['conso']['picture'] ) }}" alt=""></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h4>Các con số</h4>
                                                        @for($stt = 1; $stt < 5; $stt++)
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="">Con số</label>
                                                                    <input value="{{ @$content['cacconso_' . $stt ]['number'] }}" name="content[cacconso_{{ $stt }}][number]"  type="text" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Tiêu đề</label>
                                                                    <input value="{{ @$content['cacconso_' . $stt ]['name'] }}" name="content[cacconso_{{ $stt }}][name]"  type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    </div>

                                                </div>
                                            </section>
                                            <section>
                                                <h3>Khối các bước thực hiện</h3>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group file_picture">
                                                            <label for="">Tiêu đề</label>
                                                            <input value="{{ @$content['cacbuoc']['tieude'] }}" name="content[cacbuoc][tieude]" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    @for($stt = 1; $stt < 7; $stt++)
                                                        <div class="col-md-4">
                                                            <h4>Khối thứ {{ $stt }}</h4>
                                                            <div class="form-group file_picture">
                                                                <label for="">Hình ảnh:</label>
                                                                <input name="content[cacbuoc_{{ $stt }}][picture]" type="file" class="form-control" id="">
                                                                <p class="image-upload"><img src="{{  \App\Helper\Common::showThumb('page', @$content['cacbuoc_' . $stt]['picture'] ) }}" alt=""></p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Tiêu đề</label>
                                                                <input value="{{ @$content['cacbuoc_' . $stt ]['name'] }}" name="content[cacbuoc_{{ $stt }}][name]"  type="text" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Mô tả</label>
                                                                <textarea name="content[cacbuoc_{{ $stt }}][description]" class="form-control" cols="30" rows="5">{{ @$content['cacbuoc_' . $stt ]['description'] }}</textarea>
                                                            </div>
                                                        </div>
                                                    @endfor
                                                </div>
                                            </section>
                                            <section>
                                                <h3>Khối đánh giá</h3>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group file_picture">
                                                            <label for="">Tiêu đề</label>
                                                            <input value="{{ @$content['phamvi']['tieude'] }}" name="content[phamvi][tieude]" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    @for($stt = 1; $stt < 4; $stt++)
                                                        <div class="col-md-4">
                                                            <h4>Khối thứ {{ $stt }}</h4>
                                                            <div class="form-group file_picture">
                                                                <label for="">Hình ảnh:</label>
                                                                <input name="content[phamvi_{{ $stt }}][picture]" type="file" class="form-control" id="">
                                                                <p class="image-upload"><img src="{{  \App\Helper\Common::showThumb('page', @$content['phamvi_' . $stt]['picture'] ) }}" alt=""></p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Tiêu đề</label>
                                                                <input value="{{ @$content['phamvi_' . $stt ]['name'] }}" name="content[phamvi_{{ $stt }}][name]"  type="text" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Job</label>
                                                                <input value="{{ @$content['phamvi_' . $stt ]['job'] }}" name="content[phamvi_{{ $stt }}][job]"  type="text" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Mô tả</label>
                                                                <textarea name="content[phamvi_{{ $stt }}][description]" class="form-control" cols="30" rows="5">{{ @$content['phamvi_' . $stt ]['description'] }}</textarea>
                                                            </div>
                                                        </div>
                                                    @endfor
                                                </div>
                                            </section>
                                        </div>

                                    </div>
                                @endif
                            </div>
                            @foreach($formFields as $k => $tab)
                                <div id="{{ $k }}" class="tab-pane fade in @if($k == $keyActive) active @endif">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!! Form::show($tab['items'], $item) !!}
                                            <div class="form-group file_picture">
                                                <label for="">Hình đại diện:</label>
                                                <input name="picture" type="file" class="form-control" id="">
                                                @if(isset($item) && is_object($item)  )
                                                    <p class="image_preview"><img src="{{  \App\Helper\Common::showThumb($folderUpload, $item->picture ) }}" alt=""></p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @include("admin.template.action_form")
            </div>
            </form>
        </div>
    </div>
@stop



