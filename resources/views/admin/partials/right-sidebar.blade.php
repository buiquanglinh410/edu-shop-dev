@php

$user = Auth::user();
$permissions = [];
foreach ($user->roles as $role) {
    $tmp_permissions = json_decode($role->permissions, true);
    foreach ($tmp_permissions as $permission => $is) {
        $permissions[$permission] = $is;
    }
}
@endphp

<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{  $user->getImage() }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ $user->last_name }} {{ $user->first_name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Workshop</li>
            <li class="treeview">
                <a href="#"><i class="fa fa-print"></i><span>Bank management</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.bank.index') }}">Bank</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-print"></i><span>Pages</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.page.index') }}">Pages</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i><span>{{ __("sidebar.blog") }}</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.blog_posts.index') }}">{{ __("sidebar.blog.post") }}</a></li>

                    <li><a href="{{ route('admin.blog_categories.index') }}">{{ __("sidebar.blog.category") }}</a></li>
                    <li><a href="{{ route('admin.blog_tags.index') }}">{{ __("sidebar.blog.tags") }}</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-product-hunt"></i><span>Products</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.product_products.index') }}">Products</a></li>
                    <li><a href="{{ route('admin.product_category.index') }}">Category</a></li>
                    <li><a href="{{ route('admin.product_tags.index') }}">Tags</a></li>
                </ul>
            </li>
{{--            <li class="treeview">--}}
{{--                <a href="#"><i class="fa fa-link"></i> <span>Giáo trình</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>--}}
{{--                <ul class="treeview-menu">--}}
{{--                    <li><a href="{{ route('admin.chapter.index') }}">Chương</a></li>--}}
{{--                    <li><a href="{{ route('admin.lesson.index') }}">Bài học</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
            <li class="treeview">
                <a href="#"><i class="fa fa-product-hunt"></i><span>Course</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.course_courses.index') }}">Course</a></li>
                    <li><a href="{{ route('admin.course_category.index') }}">Category</a></li>
                    <li><a href="#">Tags</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i><span>Widgets</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.widget.index') }}">Widgets</a></li>
                    <li><a href="{{ route('admin.banner.index') }}">Banner</a></li>
                    <li><a href="{{ route('admin.notification.index') }}">Notification</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Discount</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.discount.index') }}">List Discount</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i><span>Contact</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.contact.index') }}">Contact</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-comment"></i><span>QA</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.qa_question.index') }}">Question</a></li>
                    <li><a href="{{ route('admin.qa_answer.index') }}">Answer</a></li>
                </ul>
            </li>
            <li >
                <a href="{{ route('admin.event.index') }}"><i class="fa fa-comment"></i><span>Event</span> <span class="pull-right-container"></span></a>

            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Users</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.user.index') }}">Users</a></li>
                    <li><a href="{{ route('admin.role.index') }}">Roles</a></li>
                    <li><a href="{{ route('admin.address.index') }}">Address</a></li>
                </ul>
            </li>

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
