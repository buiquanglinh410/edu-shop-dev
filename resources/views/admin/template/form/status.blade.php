<div class="form-group">
    <label>{{ $item['label'] }}:</label>
     <div class="checkbox">
        <label><input @if(old($item['name'],@$item_model->{$item['name']} ) == "active") checked @endif name="{{ $item['name'] }}" value="active" class="minimal" type="checkbox"> {{ $item['label'] }}</label>
    </div>
</div>
