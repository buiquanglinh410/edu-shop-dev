@php
$widgetfootertext  = \App\Widget::where('status','active')->where('location','text__footer')->first();
$widgets = \App\Widget::where('status','active')->whereIn('location',['footer_1','footer_2','footer_3','address','phone','mail'])->get();
$footers = [];
foreach ($widgets as $v) {
    $footers[$v->location][] = $v;
}
@endphp
<footer class="footer">
    <div class="container">
        <div class="footer-wrapper">
            <div class="footer-logo"> <img src="{{asset("enduser/assets/images/logo-light.png")}}" alt=""></div>
            <p>{!! $widgetfootertext->content !!}</p>
            <div class="footer-socials d-flex align-items-center justify-content-center">
                <div class="socials-item-wrapper d-flex align-items-center"><a class="social-item" href="#"><img src="{{asset("enduser/assets/icons/icon-facebook-white.svg")}}" alt=""></a><a class="social-item" href="#"><img src="{{asset("enduser/assets/icons/icon-twitter-white.svg")}}" alt=""></a><a class="social-item" href="#"><img src="{{asset("enduser/assets/icons/icon-instagram-white.svg")}}" alt=""></a><a class="social-item" href="#"><img src="{{asset("enduser/assets/icons/icon-youtube-white.svg")}}" alt=""></a></div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    @if(isset($footers['footer_1']))
                        @foreach($footers['footer_1'] as $v)
                            <h4>{{$v->name}}</h4>
                            {!! @$v->content !!}
                        @endforeach
                    @endif
                </div>
                <div class="col-lg-3 col-md-6">
                    @if(isset($footers['footer_2']))
                        @foreach($footers['footer_2'] as $v)
                            <h4>{{$v->name}}</h4>
                            {!! @$v->content !!}
                        @endforeach
                    @endif
                </div>
                <div class="col-lg-3 col-md-6">

                    <h4>Liên hệ</h4>
                    <ul>
                        <li class="d-flex align-items-start"><img src="{{asset("enduser/assets/icons/icon-map-marker-white.svg")}}" alt=""><a href="#">
                                @if(isset($footers['address']))
                                    @foreach($footers['address'] as $v)
                                        {{$v->name}}
                                    @endforeach
                                @endif
                            </a></li>
                        @if(isset($footers['phone']))
                            @foreach($footers['phone'] as $v)
                                <li class="d-flex align-items-center"><img src="{{asset("enduser/assets/icons/icon-phone-white.svg")}}" alt=""><a href="tel: {{$v->name}}">{{$v->name}}</a></li>
                            @endforeach
                        @endif
                        @if(isset($footers['mail']))
                            @foreach($footers['mail'] as $v)
                                <li class="d-flex align-items-center"><img src="{{asset("enduser/assets/icons/icon-mail-white.svg")}}" alt=""><a href="mailto: {{$v->name}}">{{$v->name}}</a></li>
                            @endforeach
                        @endif

                    </ul>
                </div>
                <div class="col-lg-3 col-md-6"><a class="text-left" href="#">
                        <h4>Trở thành giảng viên</h4></a>
                    <h4>Tải App</h4>
                    <div class="get-app-group d-flex align-items"><a class="get-app-item" href="#"><img src="{{asset("enduser/assets/images/image-get-on-google-play.png")}}" alt=""></a><a class="get-app-item" href="#"><img src="./assets/images/image-get-on-app-store.png" alt=""></a></div>
                </div>
            </div>
        </div>
    </div>
    <p class="footer-copy-right">© Bản quyền thuộc về <strong>Tiệm tạp hóa NHÀ MAY</strong> | Cung cấp bởi Sapo</p>
</footer>
