<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<title>GoodEdu</title>
@yield('head')
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/video.js@7.11.4/dist/video-js.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@videojs/themes@1.0.1/fantasy/index.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/videojs-playlist-ui@3.8.0/dist/videojs-playlist-ui.css">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/assets/css/main.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/assets/toastr-master/build/toastr.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/assets/css/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('enduser/assets/DatePicker-master/themes/jquery-ui.css') }}">