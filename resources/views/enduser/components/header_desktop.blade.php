<header class="section-header">
    <div class="container">
        <div class="header-wrap d-flex align-items-center justify-content-between">
            <div class="header-logo"><a href="/"><img src="{{asset('enduser/assets/images/logo-light.png')}}" alt=""></a></div>
            <form class="header-search d-flex" action="{{ route('course.search')  }}">
                <input name="search" type="text" placeholder="Bạn cần tìm gì ?">
                <button class="d-flex align-items-center justify-content-center" type="submit"><img src="{{asset('enduser/assets/icons/icon-search-white.svg')}}" alt=""></button>
            </form>
            <div class="header-icon-group d-flex align-items-center"><a class="header-collaboration header-btn" href="{{route('SiteTeacher')}}"><img src="{{asset('enduser/assets/icons/icon-collaboration-white.svg')}}" alt=""></a>
                <div class="header-wishlist header-btn">
                    <div class="circle-notification">2</div><img src="{{asset('enduser/assets/icons/icon-heart-white.svg')}}" alt="">
                </div>
                <div class="header-tickets header-btn"><img src="{{asset('enduser/assets/icons/icon-tickets-white.svg')}}" alt=""></div>
                <div class="header-carts header-btn">
                    <div class="circle-notification">{{Cart::getContent()->count()}}</div>
                    <a href="{{route('order.cart')}}"><img src="{{asset('enduser/assets/icons/icon-shopping-bag-white.svg')}}" alt=""></a>
                </div>
                <div class="header-account header-btn"><a href="{{ route('account.myProfile') }}"><img src="{{asset('enduser/assets/icons/icon-user-white.svg')}}" alt=""></a></div>
                @if(\Illuminate\Support\Facades\Auth::check())
                <div class="header-logout header-btn">
                    <a href="{{route('user.logout')}}">
                       <img src="{{asset('enduser/assets/icons/icon-logout-white.svg')}}" alt="">
                    </a>
                </div>
                @endif
            </div>
        </div>
    </div>
</header>

