<div class="menu-mobile-component">
    <div class="menu-mobile-wrapper"><img class="menu-close" src="./assets/icons/icon-close.svg" alt="">
        <form class="menu-search d-flex" action="#">
            <input type="text" placeholder="Bạn cần tìm gì ?">
            <button class="d-flex align-items-center justify-content-center" type="submit"><img src="./assets/icons/icon-search-white.svg" alt=""></button>
        </form>
        <div class="line"></div>
        <h3 class="menu-title">Liên kết</h3>
        <div class="menu-category-wrapper">
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Trang chủ</a>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Khoá học</a>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Cửa hàng</a>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Đánh giá</a>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Hướng dẫn</a>
            </div>
        </div>
        <h3 class="menu-title">Danh mục</h3>
        <div class="menu-category-wrapper">
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Sản phẩm nổi bật</a>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Sản phẩm khuyến mãi</a>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between expand-click"><span class="category-title">Nguyên liệu thêu</span><img class="icon-caret" src="./assets/icons/icon-caret-right.svg" alt="">
            </div>
            <div class="category-submenu expand-target">
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu thêu 1</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu thêu 2</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu thêu 3</a></div>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between expand-click"><span class="category-title">Vải các loại</span><img class="icon-caret" src="./assets/icons/icon-caret-right.svg" alt="">
            </div>
            <div class="category-submenu expand-target">
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Vải các loại 1</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Vải các loại 2</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Vải các loại 3</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Vải các loại 4</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Vải các loại 5</a></div>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between expand-click"><span class="category-title">Đồ may</span><img class="icon-caret" src="./assets/icons/icon-caret-right.svg" alt="">
            </div>
            <div class="category-submenu expand-target">
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Đồ may 1</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Đồ may 2</a></div>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between expand-click"><span class="category-title">Văn bản pháp quy</span><img class="icon-caret" src="./assets/icons/icon-caret-right.svg" alt="">
            </div>
            <div class="category-submenu expand-target">
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Văn bản pháp quy 1</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Văn bản pháp quy 2</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Văn bản pháp quy 3</a></div>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Dụng cụ len chọc</a>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between expand-click"><span class="category-title">Nguyên liệu khắc cao su Rubber block printing</span><img class="icon-caret" src="./assets/icons/icon-caret-right.svg" alt="">
            </div>
            <div class="category-submenu expand-target">
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu khắc cao su Rubber block printing 1</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu khắc cao su Rubber block printing 2</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu khắc cao su Rubber block printing 3</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu khắc cao su Rubber block printing 4</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu khắc cao su Rubber block printing 5</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu khắc cao su Rubber block printing 6</a></div>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between expand-click"><span class="category-title">Nguyên liệu handmade khác</span><img class="icon-caret" src="./assets/icons/icon-caret-right.svg" alt="">
            </div>
            <div class="category-submenu expand-target">
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu handmade khác 1</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu handmade khác 2</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu handmade khác 3</a></div>
                <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Nguyên liệu handmade khác 4</a></div>
            </div>
            <div class="category-item d-flex align-items-center flex-wrap justify-content-between"><a class="category-title" href="#">Mẫu rập</a>
            </div>
        </div>
    </div>
</div>
