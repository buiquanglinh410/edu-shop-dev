@php
    $category = \App\Product_category::where('status','active')->get();
@endphp
<div class="filter-item category">
    <h3>Danh mục sản phẩm </h3>
    <div class="filter-lists">
        @foreach($category as $cat)
            @php
                $link = $cat->getLink();
            @endphp
            <div class="list-item d-flex align-items-center active"><img src="{{asset("enduser/assets/icons/icon-filter.svg")}}" alt=""><span class="list-title"><a
                        href="{{ $link }}" style="color:black;">{{ $cat->name }}</a> </span><span class="list-total"></span></div>
        @endforeach
    </div>
</div>
