<script src="{{ asset('enduser/assets/js/jquery.min.js') }}"> </script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="{{ asset('enduser/assets/js/owl.carousel.min.js') }}"> </script>
<script src="{{ asset('enduser/assets/js/jquery.menu-aim.js') }}"> </script>
<script src="{{ asset('enduser/assets/js/jquery.range.js') }}"> </script>
<script src="{{ asset('enduser/assets/js/countdown-js.min.js') }}"> </script>
<script src="{{ asset('enduser/assets/js/countUp.min.js') }}"> </script>
<script src="https://cdn.jsdelivr.net/npm/video.js@7.11.4/dist/video.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/videojs-playlist@4.3.1/dist/videojs-playlist.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/videojs-playlist-ui@3.8.0/dist/videojs-playlist-ui.min.js"></script>
<script src="{{ asset('enduser/assets/js/main.js') }}"></script>
<script src="{{ asset('enduser/assets/toastr-master/build/toastr.min.js') }}"></script>
<script src="{{ asset('enduser/assets/DatePicker-master/jquery-ui.js') }}"></script>
<script src="{{ asset('enduser/assets/DatePicker-master/jquery.ui.datepicker-vi.js') }}"></script>
@yield('script')
@if(Session::has('success'))
    <script>
        toastr.success('{{ Session::get('success') }}', 'Thành công');
    </script>
@endif
<script>

    $(document).ready(function() {
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });

</script>
