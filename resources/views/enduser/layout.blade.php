<!DOCTYPE html>
<html>
<head>
    @include("enduser.components.head")
</head>
<body>
<div class="wrapper">
    @include("enduser.components.header_desktop")
    @include("enduser.components.header_mobile")
    @include("enduser.components.menu_desktop")
    @include("enduser.components.menu_mobile")
    @yield('content')
    @include("enduser.components.footer")
</div>
@include("enduser.components.script_footer")
</body>
</html>
