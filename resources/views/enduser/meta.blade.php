<title>{{ $title }}</title>
<link rel="canonical" href="{{ $link }}"/>
<meta name="description" content="{{ $description }}">
<!-- META FOR FACEBOOK -->
<meta content="{{ $title }}" property="og:site_name"/>
<meta property="og:url" itemprop="url" content="{{ $link }}"/>
<meta property="og:image" itemprop="thumbnailUrl" content="{{ $img }}"/>
<meta content="{{ $title }}" itemprop="headline" property="og:title"/>
<meta content="{{ $description }}" itemprop="description" property="og:description"/>
<!-- END META FOR FACEBOOK -->
