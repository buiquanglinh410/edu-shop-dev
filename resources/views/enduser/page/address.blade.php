@extends("enduser.layout")

@section('content')

   @include("enduser.partials.breadcrumb")

   <div class="user-layout">
       <div class="container">
           <div class="user-layout-wrapper">
               <div class="row">
                   <div class="col-lg-3">
                       @include("enduser.components.account.sidebar")
                   </div>
                   <div class="col-lg-9">
                       <div class="user-layout-main">
                           <h3 class="layout-title">Sổ địa chỉ</h3>
                           <div class="address-list-wrapper">
                               <div class="address-item">
                                   <div class="address-action d-flex align-items-center">
                                       <button class="btn edit"><img src="{{ asset('enduser/assets/icons/icon-edit-white.svg') }}" alt=""></button>
                                       <button class="btn delete"><img src="{{ asset('enduser/assets/icons/icon-trash-white.svg') }}" alt=""></button>
                                   </div>
                                   <h4 class="address-name">Your address</h4>
                                   <div class="address-col d-flex align-items-center bold"><img src="{{ asset('enduser/assets/icons/icon-home.svg') }}" alt="">Hanoi, Vietnam</div>
                                   <div class="address-col d-flex align-items-center"> <img src="{{ asset('enduser/assets/icons/icon-phone-outline.svg') }}" alt="">0123456789</div>
                               </div>
{{--                               <div class="address-item">--}}
{{--                                   <div class="address-action d-flex align-items-center">--}}
{{--                                       <button class="btn edit"><img src="./assets/icons/icon-edit-white.svg" alt=""></button>--}}
{{--                                       <button class="btn delete"><img src="./assets/icons/icon-trash-white.svg" alt=""></button>--}}
{{--                                   </div>--}}
{{--                                   <h4 class="address-name">Your address</h4>--}}
{{--                                   <div class="address-col d-flex align-items-center bold"><img src="./assets/icons/icon-home.svg" alt="">Hanoi, Vietnam</div>--}}
{{--                                   <div class="address-col d-flex align-items-center"> <img src="./assets/icons/icon-phone-outline.svg" alt="">0123456789</div>--}}
{{--                               </div>--}}
                           </div>
                           <div class="pagination-wrapper d-flex justify-content-center align-items-center flex-wrap">
                               <div class="pagination-item arrow-left">&lt;</div>
                               <div class="pagination-item active">1</div>
                               <div class="pagination-item">2</div>
                               <div class="pagination-item">3</div>
                               <div class="pagination-item arrow-right">&gt;</div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>



@stop
