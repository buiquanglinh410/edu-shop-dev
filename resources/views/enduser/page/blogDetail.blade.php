@extends("enduser.layout")

@section('content')
    @include("enduser.partials.breadcrumb")

    <div class="blog mt-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ed_blog_all_item">
                        <div class="ed_blog_item ed_bottompadder50">
                            <div class="ed_blog_image">
                                <img src="{{$new->getImage()}}" alt="blog image">
                            </div>
                        <div class="ed_blog_info">
                            <h2>{{$new->name}}</h2>
                            <ul>
{{--                                <li><a href="#"><i class="fa fa-user"></i> james marco</a></li>--}}
                                <li><a href="#"><i class="fa fa-clock-o"></i> {{$new->updated_at}}</a></li>
{{--                                <li><a href="#"><i class="fa fa-comment-o"></i> 4 comments</a></li>--}}
                            </ul>
                           {!! $new->content !!}
                        </div>
                        <div class="ed_blog_tags">
                        <ul>
                            <li><i class="fa fa-tags"></i> <a href="#">tags: </a></li>
                            @foreach($new->tags as $tag)
                            <li><a href="#">{{$tag->name}}</a></li>
                          
                                @endforeach
                        </ul>
                        <div><a href="javascript:;" id="ed_share_wrapper">share the post</a>
                            <ul id="ed_social_share">
                                <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="skype"><i class="fa fa-skype"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="google-plus"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="facebook"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                        </div>
                        </div>
                        <!--Comments section start-->
                        {{-- <div class="ed_blog_comment_wrapper">
                            <h4>All Comments</h4>
                            <div class="ed_blog_comment ed_toppadder30">
                                <div class="ed_comment_image"> <img src="images/blog/Bloger_1.jpg" alt=""> </div>
                                <div class="ed_comment_text">
                                    <h5>Frank Pascole <span>March 12, 2018 <a href="#" class="comment_reply">Reply</a></span></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula mauris ac facilisis congue. Fusce sem enim, rhoncus volutpat condimentum ac, placerat semper ligula. Suspendisse in viverra justo, eu placerat urna. Vestibulum blandit diam suscipit nibh mattis ullamcorper. Nullam a condimentum nulla, ut facilisis enim. </p>
                                </div>
                            </div>
                            <div class="ed_blog_sub_comment ed_toppadder30">
                                <div class="ed_comment_image"> <img src="images/blog/Bloger_2.jpg" alt=""> </div>
                                <div class="ed_comment_text">
                                    <h5>Tina Bonucci <span>March 13, 2018 <a href="#" class="comment_reply">Reply</a></span></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula mauris ac facilisis congue. Fusce sem enim, rhoncus volutpat condimentum ac, placerat semper ligula. Suspendisse in viverra justo, eu placerat urna. Vestibulum blandit diam suscipit nibh mattis ullamcorper. Nullam a condimentum nulla, ut facilisis enim. </p>
                                </div>
                            </div>
                            <div class="ed_blog_comment ed_toppadder30">
                                <div class="ed_comment_image"> <img src="images/blog/Bloger_3.jpg" alt=""> </div>
                                <div class="ed_comment_text">
                                    <h5>Sarah Silvester  <span>March 14, 2018 <a href="#" class="comment_reply">Reply</a></span></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula mauris ac facilisis congue. Fusce sem enim, rhoncus volutpat condimentum ac, placerat semper ligula. Suspendisse in viverra justo, eu placerat urna. Vestibulum blandit diam suscipit nibh mattis ullamcorper. Nullam a condimentum nulla, ut facilisis enim. </p>
                                </div>
                            </div>
                            <div class="ed_blog_comment ed_toppadder30">
                                <div class="ed_comment_image"> <img src="images/blog/Bloger_4.jpg" alt=""> </div>
                                <div class="ed_comment_text">
                                    <h5>Andre House <span>March 15, 2018 <a href="#" class="comment_reply">Reply</a></span></h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula mauris ac facilisis congue. Fusce sem enim, rhoncus volutpat condimentum ac, placerat semper ligula. Suspendisse in viverra justo, eu placerat urna. Vestibulum blandit diam suscipit nibh mattis ullamcorper. Nullam a condimentum nulla, ut facilisis enim. </p>
                                </div>
                            </div>
                        </div> --}}
                        <!--Comments section end-->
                        <!--Comments Form start-->
                        {{-- <div class="ed_blog_message_wrapper">
                            <h4>All Comments</h4>
                            <div class="ed_blog_messages ed_toppadder30">
                                <div class="row">
                                    <form>
                                        <div class="form-group">
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" placeholder="Your Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-4">
                                                <input type="email" class="form-control" placeholder="Your Email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-4">
                                                <input type="url" class="form-control" placeholder="Your Website URL">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <textarea class="form-control" rows="5" placeholder="Your Message"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12 text-right">
                                                <a href="#" class="btn ed_btn ed_orange">reply</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> --}}
                    <!--Comments Form end-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
