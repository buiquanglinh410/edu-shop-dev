@extends("enduser.layout")

@section('content')
    @include("enduser.partials.breadcrumb")
    @php
         $blogs = \App\blog_posts::where('status','active')->latest()->paginate(6);
    @endphp
   <div class="blog mt-4">
      <div class="container">
         <div class="row">
             @foreach($blogs as $blog)
                 @if($blog)
            <div class="col-12 col-md-6 col-lg-4">
               <a href="{{route('new.newDetail',['slug'=>$blog->slug])}}">
                  <div class="card mb-4 ">

                     <div class="box position-relative">
                        <img class="card-img-top" src="{{$blog->getImage()}}" alt="">
                        <p class="box__text-category" style="    display: inline-block;
                        position: absolute;
                        margin-bottom: 0;
                        background: #6a3073;
                        padding: 10px;
                        left: 23px;
                        color: white;
                        bottom: -13px;
                        transition: .5s;
                        font-size: 12px;
                        font-weight: 600;">

                            {{$blog->category->name}}

                      </p>
                     </div>

                     <div class="card-body">
                        <div class="entry-meta">
                           <span class="posted-on">_ <a href="#" style=" color: #7141b1;"> {{$blog->updated_at}}</a></span>
{{--                           <span class="byline">_ <a class="url fn n" href="#" style=" color: #7141b1;">Tom Black</a></span>--}}

                       </div>
                       <h4 class="card-title font-weight-bold mt-3" ><a href="{{route('new.newDetail',['slug'=>$blog->slug])}}" style="color: #1b1d21; display: inline-block;">{{$blog->name}}</a> </h4>
                       <p class="card-text ">{{$blog->description}}</p>
                         <div class="btn-readmore">
                             <a href="#"><i class="flaticon-right-arrow-1"></i>READ MORE</a>
                         </div>
                     </div>
                   </div>
               </a>

            </div>
                 @endif
             @endforeach
         </div>
      </div>
   </div>
@stop
