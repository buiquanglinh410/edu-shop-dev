@extends("enduser.layout")

@section('content')
    <style>

    </style>
   @include("enduser.partials.breadcrumb")

   <div class="cart-layout checkout-layout">
       <div class="container">
           <div class="cart-layout-wrapper">
               <h3>Thanh toán</h3>
               @if(Session::has('success'))
                   <div class="alert alert-success alert-dismissible">
                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       {{ Session::get('success') }}
                   </div>
               @endif
               <form  action="{{route('order.postCheckout')}}" method="POST">
                   @csrf
               <div class="row">
                   <div class="col-lg-7">
                       <div class="checkout-info-wrapper ">
                          <div class="authen-form">
                               <div class="form-group">
                                   <div class="form-item">
                                       <input value="{{ @$user->username }}" type="text"class="@error('name') is-invalid @enderror" name="name" placeholder="Họ và tên">
                                       @error('name')
                                       <div class="alert alert-danger  mt-2">{{ $message }}</div>
                                       @enderror
                                   </div>
                               </div>
                               <div class="form-group half">
                                   <div class="form-item">
                                       <input value="{{ @$user->email }}" type="email" class="@error('email') is-invalid @enderror" name="email" placeholder="Email">
                                       @error('email')
                                       <div class="alert alert-danger  mt-2">{{ $message }}</div>
                                       @enderror
                                   </div>
                                   <div class="form-item">
                                       <input value="{{ @$user->phone }}" type="number" class="@error('phone') is-invalid @enderror" name="phone" placeholder="Điện thoại">
                                       @error('phone')
                                       <div class="alert alert-danger  mt-2">{{ $message }}</div>
                                       @enderror
                                   </div>
                               </div>
                               @php
                               $provinces = \App\Province::orderBy('_name','asc')->get();
                               @endphp
                               <div class="form-group">
                                   <div class="form-item">
                                       @php
                                          $provinces = \App\Province::orderBy('_name','asc')->get();
                                       @endphp
                                       <select name="province_id" class="@error('province_id') is-invalid @enderror custom-select" id="select_tinh">
                                           <option value="default">Chọn tỉnh</option>
                                            @foreach($provinces as $k => $item)
                                               <option value="{{ $item->id }}">{{ $item->_name }}</option>
                                            @endforeach
                                       </select>
                                       @error('province_id')
                                       <div class="alert alert-danger  mt-2">{{ $message }}</div>
                                       @enderror
                                   </div>
                               </div>
                               <div class="form-group half">
                                   <div class="form-item">
                                       <select name="district_id" class="@error('district_id') is-invalid @enderror custom-select" id="select_quan">
                                           <option value="default">Chọn Quận/Huyện</option>
                                       </select>
                                       @error('district_id')
                                       <div class="alert alert-danger  mt-2">{{ $message }}</div>
                                       @enderror
                                   </div>
                                   <div class="form-item">
                                       <select name="ward_id" class="@error('ward_id') is-invalid @enderror custom-select" id="select_phuong">
                                           <option value="default">Chọn Phường/Xã</option>
                                       </select>
                                       @error('ward_id')
                                       <div class="alert alert-danger  mt-2">{{ $message }}</div>
                                       @enderror
                                   </div>
                               </div>
                               <div class="form-group">
                                   <div class="form-item">
                                       <input class="@error('address') is-invalid @enderror" name="address" type="text" placeholder="Địa chỉ">
                                       @error('address')
                                       <div class="alert alert-danger  mt-2">{{ $message }}</div>
                                       @enderror
                                   </div>
                               </div>
                               <div class="form-group address-list-wrapper">

                               </div>
                               <h4 class="form-title">Phương thức thanh toán</h4>
                               <div class="form-group payment-method">
                                   <div class="form-item radio">
                                       <input value="cod" type="radio" name="payment_method" id="ship-cod" checked="checked">
                                       <label class="d-flex align-items-center" for="ship-cod"><img src="{{ asset('enduser/assets/icons/icon-delivery.svg') }}" alt="">Ship COD</label>
                                   </div>
                                   <div class="form-item radio">
                                       <input value="bank" type="radio" name="payment_method" id="card">
                                       <label class="d-flex align-items-center" for="card"><img src="{{ asset('enduser/assets/icons/icon-credit-card.svg')  }}" alt="">Chuyển khoản ngân hàng</label>
                                   </div>
                               </div>
                               <div class="line"> </div>
                               <div class="form-group half">
                                   <div class="form-item"><a href="#">Quay lại giỏ hàng</a></div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-5">
                       <div class="checkout-products">
                           <div class="product-list-wrapper">
                               @php
                                   $cart = Cart::getContent();
                               @endphp
                               @if($cart->count() > 0)
                                   @foreach($cart as $v)
                                           <div class="product-checkout-item d-flex">
                                               <div class="product-image"> <img src="{{ asset('images/course_courses/'.$v['attributes']['picture']) }}" alt="">
                                                   <div class="product-amount">1</div>
                                               </div>
                                               <div class="product-info"> <a class="product-title" href="#"> {{ $v->name }}</a>
                                                   <p>Combo khóa học: Video + Gói học cụ 1</p>
                                               </div>
                                               <div class="product-price">{{ number_format($v->price* $v->quantity)  }} đ </div>
                                           </div>
                                    @endforeach
                               @endif
                               <div class="line">
                               </div>
                               <div class="checkout-code-wrapper">
                                   <p>Bạn có mã giảm giá ?</p>
                                   <div class="form-checkout-code authen-form" action="#">
                                       <div class="form-group d-flex">
                                           <input type="text" style="    flex: 1;
    width: 100%;
    height: 40px;
        padding: 0 15px;
    border: 1px solid #ddd;
    margin: 0 8px 0 0;" placeholder="Nhập mã giảm giá">
                                           <button style="    width: auto;
    margin: 0;" class="btn primary d-flex align-items-center"> <img src="{{ asset('enduser/assets/icons/icon-giftbox-white.svg') }}" alt="">Đồng ý</button>
                                       </div>
                                   </div>
                                   <p>Coupon của bạn</p>
                                   <div class="coupon-list-group">
                                       <div class="coupon-item d-flex align-items-center">
                                           <select>
                                               <option value="#">Chọn mã</option>
                                               <option value="#">Mã: DSF78</option>
                                               <option value="#">Mã: DSF79</option>
                                               <option value="#">Mã: DSF80</option>
                                           </select>
                                       </div>
                                   </div>
                               </div>
                               <div class="line"> </div>
                               <div class="checkout-line d-flex justify-content-between align-items-center">
                                   <p>Subtotal:</p><strong>{{number_format(Cart::getSubTotal()) }} đ</strong>
                               </div>
                               <div class="checkout-line d-flex justify-content-between align-items-center">
                                   <p>Phí vận chuyển:</p><strong>-</strong>
                               </div>
                               <div class="line"> </div>
                               <div class="checkout-line d-flex justify-content-between align-items-center">
                                   <p>Tổng:</p><strong class="big">{{number_format(Cart::getTotal())}} đ</strong>
                               </div>
                               <button class="btn primary w-100 mt-4" type="submit">Tiến hành thanh toán</button>
                           </div>
                       </div>
                   </div>
               </div>
               </form>
           </div>
       </div>
   </div>

@stop

@section('script')

    <script>
        $("#select_tinh").change(function(){
           var id_tinh = $(this).val();
            layHuyenTheoTinh(id_tinh)
        });
        function layHuyenTheoTinh(id_tinh){
            $("#select_quan").html('');
            $.ajax({
                url : '{{ route('ajax.getDistrict') }}?id_tinh=' + id_tinh,
                dataType:"html",
                success: function(data){
                    $("#select_quan").html(data);
                    $("#select_quan").change(function(){
                        var id_huyen = $(this).val();
                        layXaTheoHuyen(id_huyen);
                    });
                }
            });
        }
        function layXaTheoHuyen(id_huyen){
            $("#select_phuong").html('');
            $.ajax({
                url : '{{ route('ajax.getWard') }}?id_huyen=' + id_huyen,
                dataType:"html",
                success: function(data){
                    $("#select_phuong").html(data);
                }
            });
        }
    </script>

@stop
