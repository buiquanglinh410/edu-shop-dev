@extends("enduser.layout")

@section('content')
    <style>

    </style>
   @include("enduser.partials.breadcrumb")
    @php
        $banks = \App\Bank::where('status','active')->get();
    @endphp
   <div class="cart-layout checkout-layout">
       <div class="container">
           <div class="cart-layout-wrapper">
               <h3>Thanh toán thành công</h3>

               <form  action="#">
                   @csrf
               <div class="row">
                   <div class="col-lg-12">
                       <div class="checkout-info-wrapper ">
                           @if(Session::has('success'))
                               <div class="alert alert-success alert-dismissible">
                                   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                   {{ Session::get('success') }}
                               </div>
                           @endif
                       </div>
                       <h4>Thông tin đơn hàng</h4>
                       <table class="table">
                           <tr>
                               <td>Mã: </td>
                               <td>#{{ $order->id }}</td>
                           </tr>
                           <tr>
                               <td>Họ tên: </td>
                               <td>{{ $order->name }}</td>
                           </tr>
                           <tr>
                               <td>Số điện thoại </td>
                               <td>{{ $order->phone }}</td>
                           </tr>
                           <tr>
                               <td>Email </td>
                               <td>{{ $order->email }}</td>
                           </tr>
                           <tr>
                               <td>Địa chỉ </td>
                               <td>{{ $order->address }}</td>
                           </tr>
                           <tr>
                               <td>Phương thức thanh toán </td>
                               <td>{!! \App\Helper\Common::showPaymentMethod($order->pay_method) !!}</td>
                           </tr>
                       </table>
                       <h4>Chi tiết đơn hàng</h4>
                       @php
                           $details = $order->details;
                           $sum = 0;
                       @endphp
                       <table class="table">
                           <thead>
                               <th>STT</th>
                               <th>Trạng thái</th>
                               <th>Loại sản phẩm</th>
                               <th>Tên</th>
                               <th>Hình</th>
                               <th>Số lượng</th>
                               <th>Giá</th>
                               <th>Tổng</th>
                           </thead>
                           <tbody>
                           @foreach($details as $k => $detail)
                               @php
                                   $config_status = config("edushop.order_status");
                                   $total = $detail->product_price * $detail->quantity;
                                   $sum += $total;
                                   $product = $detail->getProduct; // dành cả 2 course và  học liệu
                                   $status_order_detail = \App\Helper\Common::showStatusOrder($detail->trangthai);
                                   $type_name = \App\Helper\Common::showTypeProductName($detail->type);
                               @endphp
                               <tr>
                                   <td>{{ $k + 1 }}</td>
                                   <td>{!! $status_order_detail !!}</td>
                                   <td>{!! $type_name !!}</td>
                                   <td>{{ $detail->product_name  }}</td>
                                   <td><img style="max-width: 150px" src="{{ $product->getImage() }}" alt=""></td>
                                   <td>{{ $detail->quantity  }}</td>
                                   <td>{{ number_format($detail->product_price)  }} vnđ</td>
                                   <td>{{ number_format($total) }} vnđ</td>
                               </tr>
                           @endforeach
                           <tr>
                               <td colspan="4"><h4>Tổng tiền</h4></td>
                               <td colspan="10" class="text-right"><h4>{{ number_format($sum) }} vnđ</h4></td>
                           </tr>
                           </tbody>
                       </table>
                       <h4>Danh sách tài khoản ngân hàng</h4>
                       <table class="table">
                           <thead>
                               <th>Ngân hàng</th>
                               <th>Chủ tài khoản</th>
                               <th>Số tài khoản</th>
                               <th>Chi nhánh</th>

                           </thead>
                           <tbody>
                           @foreach($banks as $b)
                               <tr>
                                   <td><img style="max-width: 50px" src="{{ $b->getImage() }}" alt=""><span>{{$b->name}}</span></td>
                                   <td>{{$b->chutaikhoan}}</td>
                                   <td>{{$b->stk}}</td>
                                   <td>{{$b->chinhanh}}</td>
                               </tr>
                               @endforeach

                           </tbody>
                       </table>
                   </div>
               </div>
               </form>
           </div>
       </div>
   </div>

@stop

@section('script')

    <script>
        $("#select_tinh").change(function(){
           var id_tinh = $(this).val();
            layHuyenTheoTinh(id_tinh)
        });
        function layHuyenTheoTinh(id_tinh){
            $("#select_quan").html('');
            $.ajax({
                url : '{{ route('ajax.getDistrict') }}?id_tinh=' + id_tinh,
                dataType:"html",
                success: function(data){
                    $("#select_quan").html(data);
                    $("#select_quan").change(function(){
                        var id_huyen = $(this).val();
                        layXaTheoHuyen(id_huyen);
                    });
                }
            });
        }
        function layXaTheoHuyen(id_huyen){
            $("#select_phuong").html('');
            $.ajax({
                url : '{{ route('ajax.getWard') }}?id_huyen=' + id_huyen,
                dataType:"html",
                success: function(data){
                    $("#select_phuong").html(data);
                }
            });
        }
    </script>

@stop
