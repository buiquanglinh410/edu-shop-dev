@extends("enduser.layout")

@section('content')
    @php
        $totalStar = $course->comments()->where('star', '>', 0)->count();
        $fiveStar = $course->comments()->where('star', 5)->count();
        $fourStar = $course->comments()->where('star', 4)->count();
        $threeStar = $course->comments()->where('star', 3)->count();
        $twoStar = $course->comments()->where('star', 2)->count();
        $oneStar = $course->comments()->where('star', 1)->count();

        if($totalStar > 0){
            $trungbinh = (5 * $fiveStar + 4 *  $fourStar + 3 * $threeStar + 2 * $twoStar +  $oneStar) / ( $fiveStar + $fourStar + $threeStar + $twoStar + $oneStar);
        }else{
            $trungbinh = 0;
        }

    @endphp
   @include("enduser.partials.breadcrumb")

   <section class="section-course-detail course-detail-infomation">
       <div class="container">
           <form action="{{ route('order.addCart') }}" method="GET" >
               @csrf
               @method('GET')
           <div class="section-course-detail-wrapper">
               <div class="row">
                   <div class="col-lg-6 col-banner">
                       <div class="course-banner-carousel">
                           @php
                           $galleries = json_decode($course->gallery);
                           @endphp
                           <div class="course-preview">
                               <div class="owl-carousel" id="course-preview-carousel">
                                   @foreach($galleries as $k => $picture)
                                       @php
                                            $src = asset("images/course_courses/" . $picture);
                                       @endphp
                                   <div class="item">
                                       <div class="course-carousel-item"><img src="{{ $src }}" alt=""></div>
                                   </div>
                                   @endforeach
                               </div>
                           </div>
                           <div class="course-control">
                               <div class="owl-carousel" id="course-control-carousel">
                                   @foreach($galleries as $k => $picture)
                                       @php
                                           $src = asset("images/course_courses/" . $picture);
                                       @endphp
                                       <div class="item">
                                           <div class="course-carousel-item"><img src="{{ $src }}" alt=""></div>
                                       </div>
                                   @endforeach
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-6 col-info">

                       <div class="course-detail-info">
                           <h2 class="course-title">{{ $course->name }}</h2>
                           <!-- Two status: stocking / out-of-stock-->
                           <p class="course-status">(<span class="stocking">Còn hàng</span>)</p>
                           <div class="line"> </div>
                           <div class="course-rating d-flex align-items-center">
                               <div class="rating-star d-flex align-items-center">
                                   @for($i = 1; $i < 6; $i++)
                                       @if($i <= round($trungbinh))
                                           <img class="star active" src="{{ asset('enduser/assets/icons/icon-star-fill-yellow.svg') }}" alt="">
                                       @else
                                           <img class="star" src="{{ asset('enduser/assets/icons/icon-star-yellow.svg') }}" alt="">
                                       @endif
                                   @endfor
                               </div>
                               <div class="rating-label">{{ round($trungbinh) }} ({{ $totalStar }} đánh giá)</div>
                           </div>
                           <h5 class="course-price">{{number_format($course->price_base)  }} đ</h5>
                           <div class="course-options d-flex flex-wrap">
                               @php
                                    $compos = $course->compos;
                               @endphp
                               @if(count($compos) > 0)

                                   @foreach($compos as $k => $compo)
                                       <div data-id="{{ $compo->id }}"class="course-option-item d-flex align-items-center justify-content-center">{{ $compo->name }}<input type="hidden" value="{{ number_format($compo->price) }}" name="price_custom"></div>
                                   @endforeach

                               @endif

                               <input type="hidden" name="compo_id" value="0">
                           </div>
                           <div class="line"></div>
                           <div class="course-buttons">
                               <div class="button-group d-flex justify-content-between half flex-wrap">
{{--                                   <input type="hidden" value="{{$course->id}}" name="id">--}}
                                       <button type="submit" class="button-item primary">Mua ngay</button>
                                       <button class="button-item" type="submit">Thêm vào giỏ hàng </button>
                                       <input type="hidden" name="id" value="{{ $course->id }}">

                               </div>
                               @if(Session::has('cartSuccess'))
                                   <div class="alert alert-success alert-dismissible w-100 mt-2">
                                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                       {{ Session::get('cartSuccess') }}
                                       <a href="{{route('order.cart')}}" >Đi đến giỏ hàng</a>
                                   </div>
                               @endif
                           </div>
                           <h4 class="course-sub-title">Thẻ</h4>
                           <div class="course-tags d-flex flex-wrap">
                               @if(isset($course_tag))
                                   @foreach($course_tag as $key => $value)
                                       <a href="{{  route('course.courseTagSlug', $value->slug) }}"><div class="tag-item">{{  $value->name }}</div></a>
                                   @endforeach
                               @endif
                           </div>
                           <h4 class="course-sub-title">Chia sẻ</h4>
                           <div class="socials-item-wrapper d-flex align-items-center flex-wrap"><a class="social-item" href="#"><img src="{{asset('enduser/assets/icons/icon-facebook-black.svg')}}" alt=""></a><a class="social-item" href="#"><img src="{{asset('enduser/assets/icons/icon-twitter-black.svg')}}" alt=""></a><a class="social-item" href="#"><img src="{{asset('enduser/assets/icons/icon-instagram-black.svg')}}" alt=""></a></div>
{{--                           <div class="course-services">--}}
{{--                               <h3>Chúng tôi bán trải nghiệm </h3>--}}
{{--                               <div class="service-item d-flex align-items-center"> <img class="item-icon" src="{{asset('enduser/assets/icons/icon-giftbox-red.svg')}}" alt="">--}}
{{--                                   <p class="item-info"><strong>Miễn phí giao hàng</strong> với đơn hàng trên 300k. Nhập mã <strong>FREEE300</strong></p>--}}
{{--                               </div>--}}
{{--                               <div class="service-item d-flex align-items-center"> <img class="item-icon" src="{{asset('enduser/assets/icons/icon-giftbox-red.svg')}}" alt="">--}}
{{--                                   <p class="item-info">Đổi trả sản phẩm <strong>7 ngày</strong> không cần lý do</p>--}}
{{--                               </div>--}}
{{--                               <div class="service-item d-flex align-items-center"> <img class="item-icon" src="{{asset('enduser/assets/icons/icon-giftbox-red.svg')}}" alt="">--}}
{{--                                   <p class="item-info"><strong>Giao hàng mới hoàn toàn</strong>, nếu hàng lỗi chỉ cần số điện thoại và địa chỉ</p>--}}
{{--                               </div>--}}
{{--                               <div class="service-item d-flex align-items-center"> <img class="item-icon" src="{{asset('enduser/assets/icons/icon-giftbox-red.svg')}}" alt="">--}}
{{--                                   <p class="item-info">Tư vấn <strong>24/7</strong></p>--}}
{{--                               </div>--}}
{{--                           </div>--}}
                           {!! $course->short_description !!}
                       </div>

                   </div>

               </div>
           </div>
           </form>
       </div>
   </section>
   <div class="section-course-tab-wrapper">
       <div class="tab-wrapper">
           <div class="container" style="position: sticky; top: 0; z-index: 2;">
               <div class="tabs-group d-flex" style="margin-bottom: 15px;"><a class="tab-item active" href="#thong-tin-chung">Thông tin chung</a><a class="tab-item" href="#ket-qua">Kết quả</a><a class="tab-item" href="#giao-trinh">Giáo trình</a><a class="tab-item" href="#hoc-lieu">Học liệu</a><a class="tab-item" href="#giang-vien">Giảng viên</a><a class="tab-item" href="#nhan-xet">Nhận xét</a></div>
           </div>
           <div class="tabs-main-group">
               <div id="thong-tin-chung" style="padding-top: 48px;">
                   <div class="tab-item">
                       <section class="section-course-detail course-detail-description">
                           <div class="container">
                               <div class="section-course-detail-wrapper">
                                   <h3 class="section-course-title">Thông tin chung</h3>
                                   <div class="description-wrapper style-content-editable">
                                       {!! $course->content !!}
                                   </div>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <div id="ket-qua" style="padding-top: 48px;">
                   <div class="tab-item">
                       <section class="section-course-detail course-detail-result">
                           <div class="container">
                               <div class="section-course-detail-wrapper">

                                   <h3 class="section-course-title">Kết quả</h3>
                                   <div class="result-wrapper">
                                    {!! $course->result !!}
                                   </div>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <div id="giao-trinh" style="padding-top: 48px;">
                   <div class="tab-item">
                       <section class="section-course-detail course-detail-lesson">
                           <div class="container">
                               <div class="section-course-detail-wrapper">
                                   <h3 class="section-course-title">Giáo trình </h3>
                                   <div class="lesson-overview-wrapper d-flex justify-content-between align-items-center">
                                       <h4 class="course-sub-title">Thông tin: </h4>
                                       <div class="lesson-overview d-flex">
                                           @php
                                                $chapters = $course->chapters;
                                                $totalLesson = \App\Helper\Common::getTotalLesson($chapters);
                                           @endphp
                                           <div class="total-item d-flex align-items-center"> <img src="./assets/icons/icon-play.svg" alt=""><span>Số bài <strong>{{ $totalLesson }}</strong></span></div>
                                           <div class="total-item d-flex align-items-center"> <img src="./assets/icons/icon-clock.svg" alt=""><span>Tổng thời gian <strong>08:55</strong></span></div>
                                       </div>
                                   </div>
                                   <div class="lesson-lists-wrapper">
                                       <div class="list-single-lesson">
                                           <div class="list-header d-flex justify-content-between align-items-center">
                                               <h3 class="list-name d-flex align-items-center"><img src="{{ asset('enduser/assets/icons/icon-play.svg') }}" alt="">Khoá học</h3>
                                               <p class="list-total">{{ $totalLesson  }} bài</p>
                                           </div>
                                           <div class="list-main">
                                               @php
                                                    $chapters = $course->chapters()->orderBy('ordering','asc')->get();
                                               @endphp
                                               @foreach($chapters as $k => $chapter)
                                                   @php
                                                        $lessons = $chapter->lessons()->orderBy('sort','asc')->get();
                                                   @endphp
                                                   <div class="list-item d-flex flex-wrap align-items-start">
                                                       <div class="item-image"><img src="{{ $chapter->getImage() }}" alt=""></div>
                                                       <div class="item-info"> <a class="item-title" href="#">{{ $chapter->name }}</a>
                                                           <ul>
                                                               @foreach($lessons as $stt => $lesson)
                                                               <li class="d-flex align-items-start"> <span class="number">{{ $stt + 1 }}</span><span class="text">{{ $lesson->label }} </span></li>
                                                                @endforeach
                                                           </ul>
                                                       </div>
                                                   </div>
                                               @endforeach
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <div id="hoc-lieu" style="padding-top: 48px;">
                   <div class="tab-item">
                       <section class="section-course-detail course-detail-result">
                           <div class="container">
                               <div class="section-course-detail-wrapper">
                                   <h3 class="section-course-title">Học liệu</h3>
                                   <div class="section-main">
                                       <div class="description-wrapper style-content-editable">
                                        {!! $course->hoclieu !!}
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <div id="giang-vien" style="padding-top: 48px;">
                   <div class="tab-item">
                       <section class="section-course-detail course-detail-result">
                           <div class="container">
                               <div class="section-course-detail-wrapper">
                                   <h3 class="section-course-title">Giảng viên</h3>
                                   <div class="section-main">
                                       <div class="description-wrapper style-content-editable">
                                           {!! $course->giangvien !!}
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <div id="nhan-xet" style="padding-top: 48px;">
                   <div class="tab-item">
                       <section class="section-course-detail course-detail-rating">
                           <div class="container">
                               <div class="section-course-detail-wrapper">

                                   <h3 class="section-course-title">Đánh giá</h3>
                                   <div class="row">
                                       <div class="col-lg-4 col-rating">
                                           <div class="rating-group-wrapper">
                                               <div class="rating-number">
                                                   <h3 class="rating-value">{{ round($trungbinh) }}</h3>
                                                   <div class="rating-star d-flex align-items-center justify-content-center">
                                                       @for($i = 1; $i < 6; $i++)
                                                           @if($i <= round($trungbinh))
                                                               <img class="star active" src="{{ asset('enduser/assets/icons/icon-star-fill-yellow.svg') }}" alt="">
                                                           @else
                                                               <img class="star" src="{{ asset('enduser/assets/icons/icon-star-yellow.svg') }}" alt="">
                                                           @endif
                                                       @endfor
                                                   </div>
                                                   <div class="rating-label">{{ round($trungbinh) }} ({{ $totalStar }} đánh giá)</div>
                                               </div>
                                               <div class="rating-process-wrapper">
                                                   <div class="process-item d-flex align-items-center justify-content-center">
                                                      @php

                                                        if($totalStar > 0){
                                                            $phantran = $fiveStar / $totalStar * 100;
                                                        }else{
                                                            $phantran = 0;
                                                        }
                                                      @endphp
                                                       <div class="rating-count-star">5</div>
                                                       <div class="rating-process">
                                                           <div class="rating-bar" style="width: {{ $phantran }}%;"></div>
                                                       </div>
                                                       <div class="rating-percent">{{ round($phantran) }}%</div>
                                                   </div>
                                                   <div class="process-item d-flex align-items-center justify-content-center">
                                                       @php

                                                            if($totalStar > 0){
                                                                 $phantran = $fourStar / $totalStar * 100;
                                                            }else{
                                                                $phantran = 0;
                                                            }
                                                       @endphp
                                                       <div class="rating-count-star">4</div>
                                                       <div class="rating-process">
                                                           <div class="rating-bar" style="width: {{ $phantran }}%;"></div>
                                                       </div>
                                                       <div class="rating-percent">{{ round($phantran) }}%</div>
                                                   </div>
                                                   <div class="process-item d-flex align-items-center justify-content-center">
                                                       @php

                                                             if($totalStar > 0){
                                                                 $phantran = $threeStar / $totalStar * 100;
                                                             }else{
                                                                 $phantran = 0;
                                                             }
                                                       @endphp
                                                       <div class="rating-count-star">3</div>
                                                       <div class="rating-process">
                                                           <div class="rating-bar" style="width: {{ $phantran }}%;"></div>
                                                       </div>
                                                       <div class="rating-percent">{{ round($phantran) }}%</div>
                                                   </div>
                                                   <div class="process-item d-flex align-items-center justify-content-center">
                                                       @php
                                                           if($totalStar > 0){
                                                                $phantran = $twoStar / $totalStar * 100;
                                                            }else{
                                                                $phantran = 0;
                                                            }

                                                       @endphp
                                                       <div class="rating-count-star">2</div>
                                                       <div class="rating-process">
                                                           <div class="rating-bar" style="width: {{ $phantran }}%;"></div>
                                                       </div>
                                                       <div class="rating-percent">{{ round($phantran) }}%</div>
                                                   </div>
                                                   <div class="process-item d-flex align-items-center justify-content-center">
                                                       @php

                                                            if($totalStar > 0){
                                                                 $phantran = $oneStar / $totalStar * 100;
                                                            }else{
                                                                $phantran = 0;
                                                            }
                                                       @endphp
                                                       <div class="rating-count-star">1</div>
                                                       <div class="rating-process">
                                                           <div class="rating-bar" style="width: {{ $phantran }}%;"></div>
                                                       </div>
                                                       <div class="rating-percent">{{ round($phantran) }}%</div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="col-lg-8 col-comments">
                                           <div class="comments-group-wrapper">
                                               <div class="comment-users-lists">
                                                   @if(count($comments) > 0)
                                                       @include("enduser.components.showComment", [ 'comments' => $comments ])
                                                   @else
                                                   <p>Chưa có đánh giá nào</p>
                                                   @endif
{{--                                                   <div class="comment-item">--}}
{{--                                                       <div class="comment-header d-flex aligns-item-center">--}}
{{--                                                           <div class="comment-avatar"> <img src="{{ asset('enduser/assets/images/image-avatar-placeholder.webp') }}" alt=""></div>--}}
{{--                                                           <div class="comment-info">--}}
{{--                                                               <h6 class="comment-name">User 5</h6>--}}
{{--                                                               <div class="rating-star d-flex align-items-center"><img src="{{ asset('assets/icons/icon-star-fill-yellow.svg') }}" alt=""><img src="{{ asset('enduser/assets/icons/icon-star-fill-yellow.svg') }}" alt=""><img src="{{ asset('enduser/assets/icons/icon-star-fill-yellow.svg') }}" alt=""><img src="{{ asset('enduser/assets/icons/icon-star-yellow.svg') }}" alt=""><img src="{{ asset('enduser/assets/icons/icon-star-yellow.') }}svg" alt=""></div>--}}
{{--                                                           </div>--}}
{{--                                                       </div>--}}
{{--                                                       <div class="comment-caption">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>--}}
{{--                                                       <div class="comment-action d-flex align-items-center">--}}
{{--                                                           <div class="action-item like d-flex align-items-center"><img src="{{ asset('enduser/assets/icons/icon-like-gray.svg') }}" alt=""><span>3</span></div>--}}
{{--                                                           <div class="action-item dislike d-flex align-items-center"><img src="{{ asset('enduser/assets/icons/icon-dislike-gray.svg') }}" alt=""><span>3</span></div>--}}
{{--                                                           <div class="action-item reply d-flex align-items-center"><img src="{{ asset('enduser/assets/icons/icon-reply-gray.svg') }}" alt=""><span>Reply</span></div>--}}
{{--                                                       </div>--}}
{{--                                                   </div>--}}
                                               </div>
                                               <h4 class="course-sub-title">Thêm đánh giá</h4>
                                               <div class="rating-star d-flex align-items-center rating-action" id="rating-action">
                                                   <img data-num="1" class="star active" src="" alt="">
                                                   <img data-num="2" class="star" src="" alt="">
                                                   <img data-num="3" class="star" src="" alt="">
                                                   <img data-num="4" class="star" src="" alt="">
                                                   <img data-num="5" class="star" src="" alt="">
                                               </div>
                                               <h4 class="course-sub-title">Đánh giá</h4>
                                               <form id="frmComment" method="POST" class="comment-form authen-form" action="{{ route('course.addComment') }}">
                                                   @csrf
                                                   <textarea name="body" placeholder="Viết 1 đánh giá"></textarea>
                                                   <div class="form-item list-uploads d-flex flex-wrap">
                                                       <div class="upload-item"> <img class="item-image" src="{{ asset('enduser/assets/images/image-sample-product.jpg') }}" alt=""><img class="item-delete" src="{{ asset('enduser/assets/icons/icon-close-circle.svg') }}" alt=""></div>
                                                       <div class="upload-item"> <img class="item-image" src="{{ asset('enduser/assets/images/image-sample-product.jpg') }}" alt=""><img class="item-delete" src="{{ asset('enduser/assets/icons/icon-close-circle.svg') }}" alt=""></div>
                                                   </div>
                                                   <div class="d-flex justify-content-between align-items-center">
                                                       <div class="form-item upload icon">
                                                           <input type="file"><img src="{{ asset('enduser/assets/icons/icon-image-gray.svg') }}" alt="">
                                                       </div>
                                                       <div class="form-item button-checkout">
                                                           <button onclick="submitFormComment(this)" class="btn" type="button">Đồng ý</button>
                                                       </div>
                                                   </div>
                                                   <input type="hidden" name="star" value="0">
                                                   <input type="hidden" name="parent_id" value="0">
                                                   <input type="hidden" name="course_id" value="{{ $course->id }}">
                                               </form>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
           </div>
       </div>
   </div>



@stop

@section('script')

    <script>
        $(".rating-action img.star").hover(function() {
            var star = $(".rating-action img.star");
            var current = $(this);
            var num = current.data('num');
            if(current.hasClass("active")){
                current.removeClass('active');
            }else{
                $.each(star,function(i,e){
                    if(i < num){
                        $(e).addClass("active");
                    }
                });
            }
            syncStar();
        }, function() {

        });
        syncStar();
        function syncStar(){
            $(".rating-action img.star").prop('src', '{{ asset('enduser/assets/icons/icon-star-yellow.svg') }}');
            $(".rating-action img.star.active").prop('src', '{{ asset('enduser/assets/icons/icon-star-fill-yellow.svg') }}');
        }
        function submitFormComment(t){
            var numStar = $(".rating-action img.star.active").length;
            $("#frmComment input[name='star']").val(numStar);
            $("#frmComment").submit();
        }
        function like(t, like, comment_id){
            var parent = $(t).parent(".comment-action");
            parent.find("input[name='like']").val(like);
            parent.find("input[name='comment_id']").val(comment_id);
            parent.find('form').submit();
        }
        $(".course-option-item").click(function(){
            $(this).parent(".course-options").find(".course-option-item").removeClass("active");
            $(this).addClass("active");
            var id = $(this).data("id");
            $("input[name='compo_id']").val(id);
            var price = $(this).find("input[name='price_custom']").val();
            $(".section-course-detail-wrapper .course-price").text(price + " đ");
        });
    </script>

@stop


