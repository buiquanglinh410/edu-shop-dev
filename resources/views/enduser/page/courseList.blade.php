@extends("enduser.layout")

@section('content')

   @include("enduser.partials.breadcrumb")
   @include("enduser.components.banner_course")
   <section class="section section-product">
       @foreach($categories as $k => $category)
       <div class="container">
           @include("enduser.partials.title_section", ['name' => $category->name, 'more' => route('course.courseListInCategory', [ 'slug_category' => $category->slug ]), 'icon'=>   asset('enduser/assets/icons/icon-book.svg')  ])
           <div class="section-main">
               <div class="row">
                   @php
                        $courses = $category->courses()->where('course_courses.status','active')->orderBy('id','desc')->limit(10)->get();
                   @endphp
                   @if(count($courses))
                       @foreach($courses as $k => $course)
                           @include("enduser.partials.item_loop_course", [ 'item' => $course,'class' => 'col-product' ])
                       @endforeach
                   @else
                    <p>Dữ liệu đang cập nhật</p>
                   @endif
               </div>
           </div>
       </div>
        @endforeach
   </section>

@stop
