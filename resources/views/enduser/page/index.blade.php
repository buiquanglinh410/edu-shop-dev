@extends("enduser.layout")

@section('content')

    <div id="isOpenCategory"></div>
    <section class="section-home-banner">
        <div class="container">
            <div class="row fix-row-no-padding">
                <div class="col-md-0 col-lg-3 no-padding"></div>
                <div class="col-md-12 col-lg-6 no-padding">
                    <div class="owl-carousel custom-carousel-style" id="home-banner-carousel">
                        @php

                          $sliders = \App\Banner::where('type', 1)->where('status','active')->where('location','home_slider')->orderBy('id','desc')->limit(4)->get();
                          $banners = \App\Banner::where('type',0)->where('status','active')->where('location','banner_home')->orderBy('id','desc')->limit(2)->get();
                          $bannerBellow = \App\Banner::where('type',0)->where('status','active')->where('location','banner_home_below_slider')->orderBy('id','desc')->first();
                        @endphp
                        @foreach($sliders as $k => $item)
                            @include("enduser.partials.item_loop_banner", [ 'item' => $item ])
                        @endforeach
                    </div>
                </div>
                <div class="col-md-12 col-lg-3 no-padding">
                    @foreach($banners as $banner)
                    <a class="image-banner-item half" href="{{$banner->link}}">
                        <img src="{{ $banner->getImage() }}" alt="{{$banner->name}}">
                    </a>
                    @endforeach
                </div>
                <div class="col-12 no-padding"><a class="image-banner-item" href="#"> <img src="{{ $bannerBellow->getImage() }}" alt="{{$bannerBellow->name}}"></a></div>
            </div>
        </div>
    </section>
{{--    <section class="section section-product">--}}
{{--        <div class="container">--}}
{{--            @include("enduser.partials.title_section", ['name' => 'Khóa học của tôi', 'icon'=>  asset("enduser/assets/icons/icon-book.svg") ])--}}
{{--            <div class="section-main">--}}
{{--                <div class="owl-carousel custom-carousel-style arrow-two-side">--}}
{{--                    @include("enduser.partials.item_loop_course", [ 'item' => [], 'class' => 'item'])--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <section class="section section-events">
        <div class="container">

            @include("enduser.partials.title_section", ['name' => 'Sự kiện', 'icon'=>  asset("enduser/assets/icons/icon-event.svg") ])
            <div class="section-main">
                @php
                    $events =\App\Event::where('status','active')->orderBy('id','desc')->take(3)->get();
                @endphp
                <div class="row">
                    @foreach($events as $event)
                        @include("enduser.partials.item_loop_event", [ 'item' => $event, 'class' => 'col-lg-4 col-md-6'  ])
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section class="section section-product">
        <div class="container">
            @include("enduser.partials.title_section", ['name' => 'Phổ biến nhất', 'icon'=>  asset("enduser/assets/icons/icon-book.svg") ])
            @php
                $courses = \App\Course_course::where('status','active')->orderBy('id','desc')->get();
            @endphp
            <div class="section-main">
                <div class="owl-carousel custom-carousel-style arrow-two-side">
                    @foreach($courses as $item)
                    @include("enduser.partials.item_loop_course", [ 'item' => $item, 'class' => 'item'])
                    @endforeach
                </div>
            </div>
        </div>
    </section>
{{--    <section class="section section-product">--}}
{{--        <div class="container">--}}
{{--            @include("enduser.partials.title_section", ['name' => 'Khuyến mãi', 'icon'=>  asset("enduser/assets/icons/icon-book.svg") ])--}}
{{--            <div class="section-main">--}}
{{--               --}}
{{--                <div class="owl-carousel custom-carousel-style arrow-two-side">--}}
{{--                    @foreach($courses as $item)--}}
{{--                        --}}
{{--                         @include("enduser.partials.item_loop_course", [ 'item' => $item, 'class' => 'item'])--}}
{{--                   --}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <section class="section section-news">
        <div class="container">
            @php
                $newVerticals = \App\blog_posts::where('status','active')->orderBy('id','desc')->get();
                $newHozion = \App\blog_posts::where('status','active')->orderBy('id','asc')->first();
            @endphp
            @include("enduser.partials.title_section", ['name' => 'Blog', 'icon'=>  asset("enduser/assets/icons/icon-news.svg") ])
            <div class="section-main">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="secion-new-wrapper lists">
                            @foreach($newVerticals as $new)
                              @include("enduser.partials.item_loop_post_vertical", [ 'item' => $new, 'class' => 'new-box-component horizontal'  ])
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-5">

                        @include("enduser.partials.item_loop_post_horizontal", [ 'item' => $newHozion, 'class' => 'secion-new-wrapper'  ])

                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
