@extends("enduser.layout")

@section('content')

   @include("enduser.partials.breadcrumb")

   <section class="section lesson-detail-video section-course-detail">
       <div class="container">
           <div class="lesson-detail-video-wrapper section-course-detail-wrapper">
               <div class="row">
                   <div class="col-lg-8 col-current-video">
                       <div class="lesson-current-video" style="text-align: center">
                           @if(count($list_lessons) != 0)
                               <video id="my-player" class="video-js vjs-theme-fantasy" controls preload="auto" poster="//vjs.zencdn.net/v/oceans.png" autoplay>
                                   <source src="//vjs.zencdn.net/v/oceans.mp4" type="video/mp4">
                                   <source src="//vjs.zencdn.net/v/oceans.webm" type="video/webm">
                                   <source src="//vjs.zencdn.net/v/oceans.ogv" type="video/ogg">
                                   <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that
                                       <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                   </p>
                               </video>
                           @else
                               <h1>Đang cập nhật dữ liệu</h1>
                           @endif
                       </div>
                   </div>
                   <div class="col-lg-4 col-list-video">
                       <div class="tab-wrapper">
                           <div class="tabs-group d-flex">
                               <div class="tab-item active">Video List</div>
                               <div class="tab-item">Hướng dẫn</div>
                           </div>
                           <div class="tabs-main-group">
                               <div class="tab-item active">
                                   <div class="lesson-list-video vjs-playlist" style="margin-top: 10px;"></div>
                                   <div class="lesson-action-video d-flex align-items-center justify-content-between">
                                       <div class="action-item-group">
                                           <input type="text" placeholder="Tìm kiếm video">
                                       </div>
                                       <div class="action-item-group d-flex align-items-center">
                                           <div class="action-item first"><img src="{{ asset('enduser/assets/icons/icon-first-track.svg') }}" alt=""></div>
                                           <div class="action-item prev"><img src="{{ asset("enduser/assets/icons/icon-first-track.svg") }}" alt=""></div>
                                           <div class="action-item next"><img src="{{ asset("enduser/assets/icons/icon-caret-right.svg") }}" alt=""></div>
                                           <div class="action-item last"><img src="{{asset("enduser/assets/icons/icon-last-track.svg")}}" alt=""></div>
                                       </div>
                                   </div>
                               </div>
                               <div class="tab-item">
                                   <div class="description-wrapper style-content-editable" style="margin-top: 10px;">
                                       <p>Để đăng ký được khóa, gói học Quý phụ huynh và các bạn học sinh thao tác các bước như sau:</p>
                                       <p><strong>1.  Nạp học phí</strong></p>
                                       <p><strong>2.  Chọn khóa, gói học</strong></p>
                                       <p><strong>3.  Xác nhận đăng ký khóa, gói học.</strong></p>
                                       <h1>GIÁ RẺ ĐẾN TẬN TAY KHÁCH HÀNG</h1>
                                       <h1>LUÔN ĐẶT UY TÍN LÊN HÀNG ĐẦU</h1>
                                       <p>— Chiếc <strong>đầm</strong> với họa tiết hoa lá gây ấn tượng kết hợp với thiết kế độc đáo ở phần cổ và vai vừa kín đáo lại vừa gợi cảm dáng váy chữ A có xếp li eo giúp phần eo trông thon gọn . Với chiếc đầm hoa phong cách cổ điển này tha hồ cho các nàng diện đi khắp mọi nơi không đối thủ và khiến cho mọi ánh nhìn phải tập trung vào mình.</p><img src="./assets/images/image-product-description-1.jpeg" alt="">
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section>
   <section class="section lesson-detail-description section-course-detail" style="padding-top: 0;">
       <div class="container">
           <div class="lesson-detail-description-wrapper section-course-detail-wrapper">
               <div class="tab-wrapper">
                   <div class="tabs-group d-flex">
                       <div class="tab-item active">Tài liệu</div>
                       <div class="tab-item">Bài kiểm tra</div>
                       <div class="tab-item">Kết quả</div>
                       <div class="tab-item">Thảo luận</div>
                   </div>
                   <div class="tabs-main-group">
                       <div class="tab-item active">
                           <div class="user-layout-table">
                               <table class="my-course-table">
                                   <thead>
                                   <tr>
                                       <td class="col-center">ID</td>
                                       <td class="course-content">Tên bài hoc</td>
                                       <td class="col-action nowrap col-center">Tài liệu</td>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   <tr>
                                       <td class="col-center">1</td>
                                       <td class="course-content">Bài 1: Tài liệu khóa học 1</td>
                                       <td class="col-action">
                                           <div class="action-group d-flex flex-wrap justify-content-center">
                                               <button class="btn primary">Tải về   </button>
                                           </div>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td class="col-center">2</td>
                                       <td class="course-content">Bài 2: Tài liệu khóa học 2</td>
                                       <td class="col-action">
                                           <div class="action-group d-flex flex-wrap justify-content-center">
                                               <button class="btn primary">Tải về   </button>
                                           </div>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td class="col-center">3</td>
                                       <td class="course-content">Bài 3: Tài liệu khóa học 3</td>
                                       <td class="col-action">
                                           <div class="action-group d-flex flex-wrap justify-content-center">
                                               <button class="btn primary">Tải về   </button>
                                           </div>
                                       </td>
                                   </tr>
                                   </tbody>
                               </table>
                           </div>
                       </div>
                       <div class="tab-item">
                           <div class="quiz-main">
                               <h3 class="quiz-title">Quiz Title </h3>
                               <div class="row quiz-wrapper">
                                   <div class="col-lg-8">
                                       <div class="quiz-wrapper-item">
                                           <div class="quiz-question">
                                               <h5>Question 1</h5>
                                               <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi ducimus nostrum sit minus, expedita quia vel dolore, odio obcaecati, sed similique porro numquam rem?</p>
                                           </div>
                                           <div class="quiz-answers">
                                               <div class="answer-item">
                                                   <input type="radio" id="a" name="quiz">
                                                   <label for="a">A </label>
                                               </div>
                                               <div class="answer-item">
                                                   <input type="radio" id="b" name="quiz">
                                                   <label for="b">B </label>
                                               </div>
                                               <div class="answer-item">
                                                   <input type="radio" id="c" name="quiz">
                                                   <label for="c">C </label>
                                               </div>
                                               <div class="answer-item">
                                                   <input type="radio" id="d" name="quiz">
                                                   <label for="d">D</label>
                                               </div>
                                           </div>
                                           <div class="quiz-action d-flex justify-content-center">
                                               <button class="btn primary prev"><img src="./assets/icons/icon-angle-left-white.svg" alt=""></button>
                                               <button class="btn primary next"><img src="./assets/icons/icon-angle-right-white.svg" alt=""></button>
                                           </div>
                                           <div class="quiz-explain">
                                               <h5>Explanation</h5>
                                               <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error iste enim quisquam. Tempora harum voluptates quas. Alias a quibusdam molestias commodi, doloremque nostrum quasi debitis maxime voluptates omnis, laborum eius.</p>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="col-lg-4 quiz-col-lists">
                                       <div class="quiz-wrapper-item quiz-lists-wrapper">
                                           <div class="quiz-total">
                                               <h5>Question 1 / 8</h5>
                                           </div>
                                           <div class="quiz-lists d-flex flex-wrap">
                                               <div class="quiz-list-item done">1</div>
                                               <div class="quiz-list-item done">2</div>
                                               <div class="quiz-list-item done">3</div>
                                               <div class="quiz-list-item correct">4</div>
                                               <div class="quiz-list-item wrong">5</div>
                                               <div class="quiz-list-item undone">6</div>
                                               <div class="quiz-list-item current">7</div>
                                               <div class="quiz-list-item undone">8</div>
                                               <div class="quiz-list-item undone">9</div>
                                               <div class="quiz-list-item undone">10</div>
                                               <div class="quiz-list-item undone">11</div>
                                               <div class="quiz-list-item undone">12</div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="tab-item">
                           <p>Need design</p>
                       </div>
                       <div class="tab-item">
                           <div class="comments-group-wrapper">
                               <div class="comment-users-lists">
                                   <div class="comment-item">
                                       <div class="comment-header d-flex aligns-item-center">
                                           <div class="comment-avatar"> <img src="./assets/images/image-avatar-placeholder.webp" alt=""></div>
                                           <div class="comment-info">
                                               <h6 class="comment-name">User 5</h6>
                                               <div class="rating-star d-flex align-items-center"><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""></div>
                                           </div>
                                       </div>
                                       <div class="comment-caption">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                       <div class="comment-action d-flex align-items-center">
                                           <div class="action-item like d-flex align-items-center"><img src="./assets/icons/icon-like-gray.svg" alt=""><span>3</span></div>
                                           <div class="action-item dislike d-flex align-items-center"><img src="./assets/icons/icon-dislike-gray.svg" alt=""><span>3</span></div>
                                           <div class="action-item reply d-flex align-items-center"><img src="./assets/icons/icon-reply-gray.svg" alt=""><span>Reply</span></div>
                                       </div>
                                   </div>
                                   <div class="comment-item">
                                       <div class="comment-header d-flex aligns-item-center">
                                           <div class="comment-avatar"> <img src="./assets/images/image-avatar-placeholder.webp" alt=""></div>
                                           <div class="comment-info">
                                               <h6 class="comment-name">User 5</h6>
                                               <div class="rating-star d-flex align-items-center"><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""></div>
                                           </div>
                                       </div>
                                       <div class="comment-caption">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                       <div class="comment-action d-flex align-items-center">
                                           <div class="action-item like d-flex align-items-center"><img src="./assets/icons/icon-like-gray.svg" alt=""><span>3</span></div>
                                           <div class="action-item dislike d-flex align-items-center"><img src="./assets/icons/icon-dislike-gray.svg" alt=""><span>3</span></div>
                                           <div class="action-item reply d-flex align-items-center"><img src="./assets/icons/icon-reply-gray.svg" alt=""><span>Reply</span></div>
                                       </div>
                                   </div>
                               </div>
                               <h4 class="course-sub-title">Bình luận</h4>
                               <form class="comment-form authen-form" action="#">
                                   <textarea placeholder="Viết 1 bình luận"></textarea>
                                   <div class="form-item list-uploads d-flex flex-wrap">
                                       <div class="upload-item"> <img class="item-image" src="./assets/images/image-sample-product.jpg" alt=""><img class="item-delete" src="./assets/icons/icon-close-circle.svg" alt=""></div>
                                       <div class="upload-item"> <img class="item-image" src="./assets/images/image-sample-product.jpg" alt=""><img class="item-delete" src="./assets/icons/icon-close-circle.svg" alt=""></div>
                                   </div>
                                   <div class="d-flex justify-content-between align-items-center">
                                       <div class="form-item upload icon">
                                           <input type="file"><img src="./assets/icons/icon-image-gray.svg" alt="">
                                       </div>
                                       <div class="form-item button-checkout">
                                           <button class="btn" type="submit">Đồng ý</button>
                                       </div>
                                   </div>
                               </form>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section>
@stop


@section('script')

    <script>
        const videoJs = {
            init: function () {
                this.configVideoJs()
            },
            configVideoJs: function () {
                const player = videojs('my-player', {
                    playbackRates: [0.7, 1.0, 1.5, 2.0],
                    nativeTextTracks: true,
                    qualitySelector: true,
                    controlBar: {
                        volumePanel: {inline: false}
                    },
                })

                player.playlist(
                    [
                        @if(isset($list_lessons))
                                @foreach($list_lessons as $key => $value)
                                    {
                                        name: '{{ $value->label }}',
                                        description: 'description',
                                        duration: {{ $value->duration === null ? 0 : $value->duration }},
                                        sources: [
                                            {
                                                src: '{{ asset('storage/lesson/video_demo/' . $value->video_demo) }}',
                                                type: 'video/mp4',
                                                label: '360P',
                                            },
                                        ],

                                        // you can use <picture> syntax to display responsive images
                                        thumbnail: [
                                            { src: '{{ $value->getImage() . '/' . $value->thumbnail }}',},
                                        ]
                                    },
                                @endforeach
                        @endif
                    ]);

                player.playlistUi()

                // const loopBtn = document.querySelector('.lesson-action-video .action-item.loop')
                const moveFirstBtn = document.querySelector('.lesson-action-video .action-item.first')
                const moveLastBtn = document.querySelector('.lesson-action-video .action-item.last')
                const movePrevBtn = document.querySelector('.lesson-action-video .action-item.prev')
                const moveNextBtn = document.querySelector('.lesson-action-video .action-item.next')

                // loopBtn.addEventListener('click', () => {
                // 	loopBtn.classList.toggle('active')
                // 	if (loopBtn.className.includes('active')) {
                // 		player.playlist.repeat(true)
                // 	} else {
                // 		player.playlist.repeat(false)
                // 	}
                // })

                moveFirstBtn.addEventListener('click', () => {
                    player.playlist.first()
                    player.play()
                })
                moveLastBtn.addEventListener('click', () => {
                    player.playlist.last()
                    player.play()
                })
                movePrevBtn.addEventListener('click', () => {
                    player.playlist.previous()
                    player.play()
                })
                moveNextBtn.addEventListener('click', () => {
                    player.playlist.next()
                    player.play()
                })
            }
        }
        $(document).ready(function () {
            videoJs.init()
        });
    </script>

@stop