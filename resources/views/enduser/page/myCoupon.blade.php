@extends("enduser.layout")

@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@silvermine/videojs-quality-selector@1.2.4/dist/css/quality-selector.css">
@stop

@section('content')

   @include("enduser.partials.breadcrumb")

   <div class="user-layout">
       <div class="container">
           <div class="user-layout-wrapper">
               <div class="row">
                   <div class="col-lg-3">
                       <div class="user-sidebar-component">
                           <div class="user-avatar"> <img src="./assets/images/image-avatar-placeholder.webp" alt="">
                               <div class="avatar-upload">
                                   <input type="file"><img src="./assets/icons/icon-image-white.svg" alt="">
                               </div>
                           </div>
                           <div class="user-name">Hello World </div>
                           <div class="user-sidebar-list"> <a class="list-item d-flex align-items-center justify-content-between" href="#"> <span>Thông tin</span><img src="./assets/icons/icon-information.svg" alt=""></a><a class="list-item d-flex align-items-center justify-content-between" href="#"> <span>Mã</span><img src="./assets/icons/icon-tickets.svg" alt=""></a><a class="list-item d-flex align-items-center justify-content-between" href="#"> <span>Yêu thích</span><img src="./assets/icons/icon-heart.svg" alt=""></a><a class="list-item d-flex align-items-center justify-content-between" href="#"> <span>Đơn hàng của tôi</span><img src="./assets/icons/icon-order.svg" alt=""></a><a class="list-item d-flex align-items-center justify-content-between" href="#"> <span>Khoá học</span><img src="./assets/icons/icon-book.svg" alt=""></a><a class="list-item d-flex align-items-center justify-content-between" href="#"> <span>Học</span><img src="./assets/icons/icon-study.svg" alt=""></a><a class="list-item d-flex align-items-center justify-content-between" href="#"> <span>Câu hỏi</span><img src="./assets/icons/icon-question.svg" alt=""></a></div>
                       </div>
                   </div>
                   <div class="col-lg-9">
                       <div class="user-layout-main">
                           <h3 class="layout-title">Mã của tôi</h3>
                           <div class="table-header-action d-flex justify-content-between">
                               <div class="action-item"></div>
                               <div class="action-item">
                                   <button class="btn primary">Thêm Coupon</button>
                               </div>
                           </div>
                           <div class="user-layout-table">
                               <table class="my-course-table">
                                   <thead>
                                   <tr>
                                       <td class="col-center">STT</td>
                                       <td class="col-center nowrap">Số series</td>
                                       <td class="course-content">Khoá học áp dụng</td>
                                       <td class="col-center nowrap">Giá</td>
                                       <td class="col-center nowrap">Đơn vị</td>
                                       <td class="col-center">Trạng thái</td>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   <tr>
                                       <td class="col-center">1</td>
                                       <td class="col-center nowrap">123-456-789</td>
                                       <td class="course-content">English</td>
                                       <td class="col-center nowrap">200000</td>
                                       <td class="col-center nowrap">VND</td>
                                       <td class="tags">
                                           <div class="tag-group d-flex flex-wrap justify-content-center">
                                               <div class="tag-item active">active</div>
                                           </div>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td class="col-center">2</td>
                                       <td class="col-center nowrap">123-456-789</td>
                                       <td class="course-content">English</td>
                                       <td class="col-center nowrap">10</td>
                                       <td class="col-center nowrap">%</td>
                                       <td class="tags">
                                           <div class="tag-group d-flex flex-wrap justify-content-center">
                                               <div class="tag-item unactive">unactive</div>
                                           </div>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td class="col-center">3</td>
                                       <td class="col-center nowrap">123-456-789</td>
                                       <td class="course-content">English</td>
                                       <td class="col-center nowrap">200000</td>
                                       <td class="col-center nowrap">VND</td>
                                       <td class="tags">
                                           <div class="tag-group d-flex flex-wrap justify-content-center">
                                               <div class="tag-item waiting">waiting</div>
                                           </div>
                                       </td>
                                   </tr>
                                   </tbody>
                               </table>
                           </div>
                           <div class="pagination-wrapper d-flex justify-content-center align-items-center flex-wrap">
                               <div class="pagination-item arrow-left">&lt;</div>
                               <div class="pagination-item active">1</div>
                               <div class="pagination-item">2</div>
                               <div class="pagination-item">3</div>
                               <div class="pagination-item arrow-right">&gt;</div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>


@stop
