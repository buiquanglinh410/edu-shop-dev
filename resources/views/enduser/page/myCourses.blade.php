@extends("enduser.layout")

@section('content')

   @include("enduser.partials.breadcrumb")

   <div class="user-layout">
       <div class="container">
           <div class="user-layout-wrapper">
               <div class="row">
                   <div class="col-lg-3">
                       @include("enduser.components.account.sidebar")
                   </div>
                   <div class="col-lg-9">
                       <div class="user-layout-main">
                           <h3 class="layout-title">Khoá học của tôi</h3>
                           <div class="section-main" style="margin-bottom: 15px">
                               <div class="row">
                                   @if($list_user_lesson->count() > 0)
                                       @foreach($list_user_lesson as $key => $value)
                                           @if($value->status == 'active')
                                                   <div class="col-lg-4 col-sm-6">
                                                       <div class="product-component">
                                                           <div class="product-image"> <a href="{{ route('course.lessionDetailChapter', $value->slug) }}"><img src="{{  asset('/images/course_courses/'. $value->picture) }}" alt=""></a>
                                                           </div><a class="product-title" href="{{ route('course.lessionDetailChapter', $value->slug) }}">{{ $value->name }}</a>
                                                           <div class="line"> </div>
                                                           <div class="product-progress-wrapper">
                                                               <div class="product-progress-label d-flex justify-content-between">
                                                                   <p>Hoàn thiện:</p>
                                                                   <p>88%</p>
                                                               </div>
                                                               <div class="product-progress">
                                                                   <div class="progress-bar" style="width: 88%;"></div>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                           @endif
                                       @endforeach
                                   @endif

                               </div>
                           </div>
{{--                           <div class="pagination-wrapper d-flex justify-content-center align-items-center flex-wrap">--}}
{{--                               <div class="pagination-item arrow-left">&lt;</div>--}}
{{--                               <div class="pagination-item active">1</div>--}}
{{--                               <div class="pagination-item">2</div>--}}
{{--                               <div class="pagination-item">3</div>--}}
{{--                               <div class="pagination-item arrow-right">&gt;</div>--}}
{{--                           </div>--}}
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>


@stop
