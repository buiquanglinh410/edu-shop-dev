@extends("enduser.layout")

@section('content')

   @include("enduser.partials.breadcrumb")

   <div class="user-layout">
       <div class="container">
           <div class="user-layout-wrapper">
               <div class="row">
                   <div class="col-lg-3">
                       @include("enduser.components.account.sidebar")
                   </div>
                   <div class="col-lg-9">
                       <div class="user-layout-main">
                           <h3 class="layout-title">Thông tin cá nhân</h3>
                           @if(Session::has('success'))
                           <div class="alert alert-success alert-dismissible">
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                               {{ Session::get('success') }}
                           </div>
                           @endif
                           <form lang="vn" method="POST" class="authen-form" action="{{ route('account.changeProfile') }}">
                               @csrf
                               <div class="form-group half">
                                   <div class="form-item">
                                       <label>Họ:</label>
                                       <input name="first_name" value="{{ $user->first_name }}" type="text" placeholder="Họ">
                                       @error('first_name')
                                       <p class="form-error">{{ $message }}</p>
                                       @enderror
                                   </div>
                                   <div class="form-item">
                                       <label>Tên:</label>
                                       <input name="last_name" value="{{ $user->last_name }}" type="text" placeholder="Tên">
                                       @error('last_name')
                                       <p class="form-error">{{ $message }}</p>
                                       @enderror
                                   </div>
                               </div>
                               <div class="form-group half">
                                   <div class="form-item">
                                       <label>Ngày sinh:</label>
                                       <input class="datepicker" name="birthday" value="{{ implode("/", array_reverse(explode("-", $user->birthday))) }}" placeholder="Ngày sinh">
                                       @error('birthday')
                                       <p class="form-error">{{ $message }}</p>
                                       @enderror
                                   </div>
                                   <div class="form-item">
                                       <label>Email:</label>
                                       <input value="{{ $user->email }}" type="text" placeholder="Email" disabled="disabled"">
                                   </div>
                               </div>
                               <div class="form-group half">
                                   <div class="form-item">
                                       <label>Số điện thoại</label>
                                       <input name="phone" value="{{ $user->phone }}" type="text" placeholder="Số điện thoại">
                                       @error('phone')
                                        <p class="form-error">{{ $message }}</p>
                                       @enderror
                                   </div>
                               </div>
                               <div class="form-group half">
                                   <div class="form-item"></div>
                                   <div class="form-item button-checkout">
                                       <button type="submit" class="btn primary">Cập nhật </button>
                                   </div>
                               </div>
                           </form>

{{--                           <h3 class="layout-title">Sổ địa chỉ</h3>--}}
{{--                           <div class="address-list-wrapper">--}}
{{--                               <div class="address-item">--}}
{{--                                   <div class="address-action d-flex align-items-center">--}}
{{--                                       <button class="btn edit"><img src="./assets/icons/icon-edit-white.svg" alt=""></button>--}}
{{--                                       <button class="btn delete"><img src="./assets/icons/icon-trash-white.svg" alt=""></button>--}}
{{--                                   </div>--}}
{{--                                   <h4 class="address-name">Your address</h4>--}}
{{--                                   <div class="address-col d-flex align-items-center bold"><img src="./assets/icons/icon-home.svg" alt="">Hanoi, Vietnam</div>--}}
{{--                                   <div class="address-col d-flex align-items-center"> <img src="./assets/icons/icon-phone-outline.svg" alt="">0123456789</div>--}}
{{--                               </div>--}}
{{--                               <div class="address-item">--}}
{{--                                   <div class="address-action d-flex align-items-center">--}}
{{--                                       <button class="btn edit"><img src="./assets/icons/icon-edit-white.svg" alt=""></button>--}}
{{--                                       <button class="btn delete"><img src="./assets/icons/icon-trash-white.svg" alt=""></button>--}}
{{--                                   </div>--}}
{{--                                   <h4 class="address-name">Your address</h4>--}}
{{--                                   <div class="address-col d-flex align-items-center bold"><img src="./assets/icons/icon-home.svg" alt="">Hanoi, Vietnam</div>--}}
{{--                                   <div class="address-col d-flex align-items-center"> <img src="./assets/icons/icon-phone-outline.svg" alt="">0123456789</div>--}}
{{--                               </div>--}}
{{--                           </div>--}}
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>



@stop
