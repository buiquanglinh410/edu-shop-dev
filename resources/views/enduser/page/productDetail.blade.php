@extends("enduser.layout")

@section('content')

   @include("enduser.partials.breadcrumb")

   <section class="section-course-detail course-detail-infomation">
       <div class="container">
           <form action="{{ route('order.addCart') }}?type=product" method="GET" >
               @csrf
               @method('GET')
           <div class="section-course-detail-wrapper">
               <div class="row">
                   <div class="col-lg-6 col-banner">
                       <div class="course-banner-carousel">
                           @php
                                $galleries = json_decode($product->gallery,true);
                           @endphp
                           <div class="course-preview">
                               <div class="owl-carousel" id="course-preview-carousel">
                                   @foreach($galleries as $k => $picture)
                                   <div class="item">
                                       <div class="course-carousel-item"><img src="{{ asset('images/product_products/' . $picture) }}" alt=""></div>
                                   </div>
                                   @endforeach
                               </div>
                           </div>
                           <div class="course-control">
                               <div class="owl-carousel" id="course-control-carousel">
                                   @foreach($galleries as $k => $picture)
                                       <div class="item">
                                           <div class="course-carousel-item"><img src="{{ asset('images/product_products/' . $picture) }}" alt=""></div>
                                       </div>
                                   @endforeach
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-6 col-info">
                       <div class="course-detail-info">
                           <h2 class="course-title">{{ mb_strtoupper($product->name) }}</h2>
                           <!-- Two status: stocking / out-of-stock-->
                           <p class="course-status">(<span class="stocking">Còn hàng</span>)</p>
                           <div class="line"> </div>
                           <div class="course-rating d-flex align-items-center">
                               <div class="rating-star d-flex align-items-center">
                                   <img src="{{ asset('enduser/assets/icons/icon-star-fill-yellow.svg') }}" alt="">
                                   <img src="{{ asset('enduser/assets/icons/icon-star-fill-yellow.svg') }}" alt="">
                                   <img src="{{ asset('enduser/assets/icons/icon-star-fill-yellow.svg') }}" alt="">
                                   <img src="{{ asset('enduser/assets/icons/icon-star-yellow.svg') }}" alt="">
                                   <img src="{{ asset('enduser/assets/icons/icon-star-yellow.svg') }}" alt="">
                               </div>
                               <div class="rating-label">4.5 (3 đánh giá)</div>
                           </div>
                           <h5 class="course-price">{{ number_format($product->price_final) }} đ</h5>
                           <div class="course-options d-flex flex-wrap">
{{--                               <div class="course-option-item d-flex align-items-center justify-content-center active">Video</div>--}}
{{--                               <div class="course-option-item d-flex align-items-center justify-content-center">Video + Gói học cụ 1</div>--}}
{{--                               <div class="course-option-item d-flex align-items-center justify-content-center">Video + Gói học cụ 2</div>--}}
{{--                               <div class="course-option-item d-flex align-items-center justify-content-center">Video + Gói học cụ 3</div>--}}
{{--                               <div class="course-option-item d-flex align-items-center justify-content-center">Video + Gói học cụ 4</div>--}}
{{--                               <div class="course-option-item d-flex align-items-center justify-content-center">Video + Gói học cụ 5</div>--}}
                           </div>
                           <div class="line"></div>
                           <div class="course-buttons">
                               <div class="button-group d-flex justify-content-between half flex-wrap">
                                   <button type="submit" class="button-item primary">Mua ngay</button>
                                   <button class="button-item" type="submit">Thêm vào giỏ hàng </button>
                                   <input type="hidden" name="type" value="product">
                                   <input type="hidden" name="id" value="{{ $product->id }}">
                               </div>
                               @if(Session::has('cartSuccess'))
                                   <div class="alert alert-success alert-dismissible w-100 mt-2">
                                       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                       {{ Session::get('cartSuccess') }}
                                       <a href="{{route('order.cart')}}">Đi đến giỏ hàng</a>
                                   </div>
                               @endif
                           </div>
                           <h4 class="course-sub-title">Thẻ</h4>
                           <div class="course-tags d-flex flex-wrap">
                               @if(isset($product_tag))
                                   @foreach($product_tag as $key => $value)
                                       <a href="#"><div class="tag-item">{{  $value->name }}</div></a>
                                   @endforeach
                               @endif
                           </div>
                           <h4 class="course-sub-title">Chia sẻ</h4>
                           <div class="socials-item-wrapper d-flex align-items-center flex-wrap"><a class="social-item" href="#"><img src="{{ asset('enduser/assets/icons/icon-facebook-black.svg') }}" alt=""></a><a class="social-item" href="#"><img src="{{ asset('enduser/assets/icons/icon-twitter-black.svg') }}" alt=""></a><a class="social-item" href="#"><img src="{{ asset('enduser/assets/icons/icon-instagram-black.svg') }}" alt=""></a></div>
                           <div class="course-services">
                               <h3>Chúng tôi bán trải nghiệm </h3>
                               <div class="service-item d-flex align-items-center"> <img class="item-icon" src="{{ asset('enduser/assets/icons/icon-giftbox-red.svg') }}" alt="">
                                   <p class="item-info"><strong>Miễn phí giao hàng</strong> với đơn hàng trên 300k. Nhập mã <strong>FREEE300</strong></p>
                               </div>
                               <div class="service-item d-flex align-items-center"> <img class="item-icon" src="{{ asset('enduser/assets/icons/icon-giftbox-red.svg') }}" alt="">
                                   <p class="item-info">Đổi trả sản phẩm <strong>7 ngày</strong> không cần lý do</p>
                               </div>
                               <div class="service-item d-flex align-items-center"> <img class="item-icon" src="{{ asset('enduser/assets/icons/icon-giftbox-red.svg') }}" alt="">
                                   <p class="item-info"><strong>Giao hàng mới hoàn toàn</strong>, nếu hàng lỗi chỉ cần số điện thoại và địa chỉ</p>
                               </div>
                               <div class="service-item d-flex align-items-center"> <img class="item-icon" src="{{ asset('enduser/assets/icons/icon-giftbox-red.svg') }}" alt="">
                                   <p class="item-info">Tư vấn <strong>24/7</strong></p>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
           </form>
       </div>
   </section>
   <div class="product-section-info" style="margin-bottom: 15px;">
       <div class="container">
           <div class="row">
               <div class="col-lg-8 no-padding">
                   <div class="tab-wrapper">
                       <div class="tabs-group d-flex" style="padding: 0 15px; position: sticky; top: 0; z-index: 2;margin-bottom: -20px;"><a class="tab-item active" href="#thong-tin-chung">Thông tin chung</a><a class="tab-item" href="#mo-ta">Mô tả sản phẩm</a></div>
                       <div class="tabs-main-group">
                           <div id="thong-tin-chung" style="padding-top: 48px;">
                               <div class="tab-item">
                                   <section class="section-course-detail course-detail-description">
                                       <div class="container">
                                           <div class="section-course-detail-wrapper">
                                               <h3 class="section-course-title">Thông tin chung</h3>
                                               <div class="description-wrapper style-content-editable">
                                                   <p><strong>Bạn đang mong muốn xây dựng một Hệ thống Kinh doanh Online Bài Bản cho riêng mình?</strong> mà vẫn chưa tìm được hướng đi rõ ràng từ việc xác định sản phẩm kinh doanh - mô hình kinh doanh phù hợp, cách để liên hệ nhà cung cấp để đàm phán nhập hàng, cách nghiên cứu khách hàng, đối thủ, quảng cáo và tối ưu...... ?</p>
                                                   <p>Bạn đã có kinh nghiệm Kinh doanh online, và đang mong muốn mở rộng Hệ Thống Online Đa Kênh ngoài kênh truyền thống sang Facebook, Instagram, Zalo, Youtube, Email Marketing, Website, SMS...?</p>
                                                   <p>Bạn mong muốn phát triển toàn diện nội dung fanpage, group,.... Bài bản hơn bắt đầu từ việc phân tích thị hiếu khách hàng - cũng như phân tích toàn diện đối thủ cạnh tranh của mình ?</p>
                                                   <p>Bạn đam mê Marketing và muốn lên một quy trình Quảng cáo sản phẩm online bài bản, hay mong muốn tối ưu quảng cáo hiệu quả ? Cho dù bạn là người mới Khởi nghiệp Kinh Doanh Online, hay là một nhân viên kinh doanh hoặc bạn đã có kinh nghiệm kinh doanh Online; có sản phẩm dịch vụ và muốn mở rộng công việc kinh doanh của mình hay đơn giản bạn muốn kinh doanh để có thêm nguồn thu nhập cho bản thân thì đây là khóa chia sẻ dành cho bạn ! Tất cả những vấn đề trên sẽ được mổ sẻ trong "Khởi nghiệp Kinh Doanh Online Bền Vững" - Khóa học này cung cấp cho bạn một Quy trình xây dựng Hệ thống Marketing online thực tế, bài bản, toàn diện với những ví dụ thực tế.</p>
                                                   <p><strong>Đừng chần chừ nữa, hãy đăng ký tham gia ngay cùng tôi. Và chuẩn bị đón nhận những món quà đầy bất ngờ.</strong></p>
                                                   <h3>Lộ trình học tập:</h3>
                                                   <p>217 bài học chia làm 12 chương giúp hịc viện hiểu rõ hơn về các công cụ Marketing mới nhât hiện nay.</p>
                                                   <h3>Lợi ích từ khoá học</h3>
                                                   <p>Xác định Sản phẩm kinh doanh phù hợp, Mô hình bạn sẽ Kinh doanh</p>
                                                   <p>Có được Nguồn hàng kinh doanh khi liên hệ trực tiếp với người bán, nhà cung cấp</p>
                                                   <p>Nhận diện chân dung khách hàng Tiềm năng của mình, cách xây dựng chính sách chăm sóc khách hàn</p>
                                                   <p>Cách nghiên cứu, theo dõi và học hỏi từ chính Đối thủ cạnh tranh của bạn qua các công cụ Miễn phí</p>
                                                   <p>Tự tin xây dựng đồng bộ các kênh Marketing (Facebook, Instagram, Zalo, Youtube, Email Marketing,...) qua từng bước chia sẻ chi tiết</p>
                                                   <h3>Lợi ích từ khoá học</h3>
                                                   <p>Tất cả những ai muốn khởi nghiệp Kinh doanh Online bài bản, bắt đầu từ những công việc cốt lõi nhất: xác định sản phẩm kinh doanh, tìm kiếm nguồn hàng kinh doanh, liên hệ nhà cung cấp, nghiên cứu khách hàng, đối thủ, xây dựng nội dung bán hàng...</p>
                                                   <p>Những ai mong muốn tìm hiểu, phát triển đồng bộ các kênh Marketing Online (Instagram, Zalo, Youtube, Email Marketing, Facebook, SMS) bên cạnh kênh truyền thống</p>
                                                   <p>Những ai đang tìm kiếm các cơ hội kinh doanh Online, và phát triển công việc Kinh Doanh Offline bền vững cho riêng mình</p>
                                               </div>
                                           </div>
                                       </div>
                                   </section>
                               </div>
                           </div>
                           <div id="mo-ta" style="padding-top: 48px;">
                               <div class="tab-item">
                                   <section class="section-course-detail course-detail-description">
                                       <div class="container">
                                           <div class="section-course-detail-wrapper">
                                               <h3 class="section-course-title">Mô tả sản phẩm</h3>
                                               <div class="description-wrapper style-content-editable">
                                                   <h1>GIÁ RẺ ĐẾN TẬN TAY KHÁCH HÀNG</h1>
                                                   <h1>LUÔN ĐẶT UY TÍN LÊN HÀNG ĐẦU</h1>
                                                   <p>— Chiếc <strong>đầm</strong> với họa tiết hoa lá gây ấn tượng kết hợp với thiết kế độc đáo ở phần cổ và vai vừa kín đáo lại vừa gợi cảm dáng váy chữ A có xếp li eo giúp phần eo trông thon gọn . Với chiếc đầm hoa phong cách cổ điển này tha hồ cho các nàng diện đi khắp mọi nơi không đối thủ và khiến cho mọi ánh nhìn phải tập trung vào mình.</p><img src="./assets/images/image-product-description-1.jpeg" alt="">
                                                   <h3>Gợi ý khi nhận sản phẩm</h3>
                                                   <p>Kiểm tra sản phẩm xem có lỗi không</p>
                                                   <p>Thử xem có vừa vặn không</p>
                                                   <p>Giặt tay riêng với các loại quần áo khác, nhất là sản phẩm có màu đen, đỏ </p>
                                                   <p>Không giặt hoặc ngâm xà phòng ngay trong lần đầu tiên</p>
                                                   <p>Nên giặt trước khi mặc nhằm tránh dị ứng bụi chỉ cho làn da nhạy cảm nhất</p>
                                                   <h3>* Lưu Ý </h3>
                                                   <p>Tuy nhiên các nàng cũng thông cảm giúp shop suy nghĩ kỹ trước khi quyết định bấm mua hàng nhé, để tránh tình trạng đổi ý vì bất kì lí do gì. Bởi vì khi hủy đơn không chỉ tội cho nhân viên giao hàng tốn sức chạy, tốn tiền gọi giao mà cũng tội cho shop tốn phí ship, bị điểm phạt giao hàng không thành công, dẫn đến nguy cơ cấm bán hàng chị em nhé!</p>
                                                   <p>Với rất nhiều kiểu thiết kế và màu sắc khác nhau, phù hợp với nhiều sở thích của các nàng. Vì thế, bạn sẽ không quá khó để lựa chọn một mẫu ưng ý cho tủ đồ của mình tại đây các nàng nhé : https://www.sendo.vn/shop/nomax</p><img src="./assets/images/image-product-description-2.jpeg" alt="">
                                               </div>
                                           </div>
                                       </div>
                                   </section>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col-lg-4 no-padding">
                   <section class="section-course-detail course-detail-same-product">
                       <div class="container">
                           <div class="section-course-detail-wrapper">
                               <h3 class="section-course-title">Sản phẩm bán chạy</h3>
                               <div class="section-main">
                                   <div class="row">
                                       @include("enduser.components.product_upsale")
                                   </div>
                               </div>
                           </div>
                       </div>
                   </section>
               </div>
           </div>
       </div>
   </div>
   <section class="section-course-detail course-detail-rating">
       <div class="container">
           <div class="section-course-detail-wrapper">
               <h3 class="section-course-title">Đánh giá</h3>
               <div class="row">
                   <div class="col-lg-4 col-rating">
                       <div class="rating-group-wrapper">
                           <div class="rating-number">
                               <h3 class="rating-value">4.5</h3>
                               <div class="rating-star d-flex align-items-center justify-content-center"><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""></div>
                               <div class="rating-label">4.5 (3 đánh giá)</div>
                           </div>
                           <div class="rating-process-wrapper">
                               <div class="process-item d-flex align-items-center justify-content-center">
                                   <div class="rating-count-star">5</div>
                                   <div class="rating-process">
                                       <div class="rating-bar" style="width: 50%;"></div>
                                   </div>
                                   <div class="rating-percent">50%</div>
                               </div>
                               <div class="process-item d-flex align-items-center justify-content-center">
                                   <div class="rating-count-star">4</div>
                                   <div class="rating-process">
                                       <div class="rating-bar" style="width: 30%;"></div>
                                   </div>
                                   <div class="rating-percent">30%</div>
                               </div>
                               <div class="process-item d-flex align-items-center justify-content-center">
                                   <div class="rating-count-star">3</div>
                                   <div class="rating-process">
                                       <div class="rating-bar" style="width: 0%;"></div>
                                   </div>
                                   <div class="rating-percent">0%</div>
                               </div>
                               <div class="process-item d-flex align-items-center justify-content-center">
                                   <div class="rating-count-star">2</div>
                                   <div class="rating-process">
                                       <div class="rating-bar" style="width: 20%;"></div>
                                   </div>
                                   <div class="rating-percent">20%</div>
                               </div>
                               <div class="process-item d-flex align-items-center justify-content-center">
                                   <div class="rating-count-star">1</div>
                                   <div class="rating-process">
                                       <div class="rating-bar" style="width: 00%;"></div>
                                   </div>
                                   <div class="rating-percent">00%</div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-8 col-comments">
                       <div class="comments-group-wrapper">
                           <div class="comment-users-lists">
                               <div class="comment-item">
                                   <div class="comment-header d-flex aligns-item-center">
                                       <div class="comment-avatar"> <img src="./assets/images/image-avatar-placeholder.webp" alt=""></div>
                                       <div class="comment-info">
                                           <h6 class="comment-name">User 5</h6>
                                           <div class="rating-star d-flex align-items-center"><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""></div>
                                       </div>
                                   </div>
                                   <div class="comment-caption">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                   <div class="comment-action d-flex align-items-center">
                                       <div class="action-item like d-flex align-items-center"><img src="./assets/icons/icon-like-gray.svg" alt=""><span>3</span></div>
                                       <div class="action-item dislike d-flex align-items-center"><img src="./assets/icons/icon-dislike-gray.svg" alt=""><span>3</span></div>
                                       <div class="action-item reply d-flex align-items-center"><img src="./assets/icons/icon-reply-gray.svg" alt=""><span>Reply</span></div>
                                   </div>
                               </div>
                               <div class="comment-item">
                                   <div class="comment-header d-flex aligns-item-center">
                                       <div class="comment-avatar"> <img src="./assets/images/image-avatar-placeholder.webp" alt=""></div>
                                       <div class="comment-info">
                                           <h6 class="comment-name">User 5</h6>
                                           <div class="rating-star d-flex align-items-center"><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""></div>
                                       </div>
                                   </div>
                                   <div class="comment-caption">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
                                   <div class="comment-action d-flex align-items-center">
                                       <div class="action-item like d-flex align-items-center"><img src="./assets/icons/icon-like-gray.svg" alt=""><span>3</span></div>
                                       <div class="action-item dislike d-flex align-items-center"><img src="./assets/icons/icon-dislike-gray.svg" alt=""><span>3</span></div>
                                       <div class="action-item reply d-flex align-items-center"><img src="./assets/icons/icon-reply-gray.svg" alt=""><span>Reply</span></div>
                                   </div>
                               </div>
                           </div>
                           <h4 class="course-sub-title">Thêm đánh giá</h4>
                           <div class="rating-star d-flex align-items-center" id="rating-action"><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-fill-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""><img src="./assets/icons/icon-star-yellow.svg" alt=""></div>
                           <h4 class="course-sub-title">Đánh giá</h4>
                           <form class="comment-form authen-form" action="#">
                               <textarea placeholder="Viết 1 đánh giá"></textarea>
                               <div class="form-item list-uploads d-flex flex-wrap">
                                   <div class="upload-item"> <img class="item-image" src="./assets/images/image-sample-product.jpg" alt=""><img class="item-delete" src="./assets/icons/icon-close-circle.svg" alt=""></div>
                                   <div class="upload-item"> <img class="item-image" src="./assets/images/image-sample-product.jpg" alt=""><img class="item-delete" src="./assets/icons/icon-close-circle.svg" alt=""></div>
                               </div>
                               <div class="d-flex justify-content-between align-items-center">
                                   <div class="form-item upload icon">
                                       <input type="file"><img src="./assets/icons/icon-image-gray.svg" alt="">
                                   </div>
                                   <div class="form-item button-checkout">
                                       <button class="btn" type="submit">Đồng ý</button>
                                   </div>
                               </div>
                           </form>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section>
   <section class="section section-product">
       <div class="container">
           <div class="section-header d-flex align-items-center justify-content-between">
               <h2 class="d-flex align-items-center"> <img src="{{ asset('enduser/assets/icons/icon-book.svg') }}" alt="">Sản phẩm gần đây
               </h2><a class="d-flex align-items-center" href="#">Xem tất cả<img src="./assets/icons/icon-angle-right.svg" alt=""></a>
           </div>
           <div class="section-main">
               <div class="owl-carousel custom-carousel-style arrow-two-side">
                  @include("enduser.components.product_recently")
               </div>
           </div>
       </div>
   </section>




@stop
