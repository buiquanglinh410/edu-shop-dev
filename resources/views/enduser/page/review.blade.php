@extends("enduser.layout")

@section('content')

   @include("enduser.partials.breadcrumb")

   <section class="section section-review" style="padding-top: 0;">
       <div class="container">
           <div class="section-header d-flex align-items-center justify-content-between">
               <h2 class="d-flex align-items-center"> <img src="./assets/icons/icon-review.svg" alt="">Đánh giá</h2>
               <div class="sort-group d-flex align-items-center">
                   <label>Sort by</label>
                   <select class="custom-select">
                       <option value="#">Danh mục </option>
                       <option value="#">Danh mục 1</option>
                       <option value="#">Danh mục 2</option>
                       <option value="#">Danh mục 3</option>
                       <option value="#">Danh mục 4</option>
                   </select>
               </div>
           </div>
           <div class="section-main">
               <div class="row">
                   <div class="col-lg-3 col-md-4 col-sm-6">
                       <div class="review-component">
                           <div class="review-image"> <img src="./assets/images/image-sample-review.jpeg" alt=""></div>
                           <div class="review-info">
                               <h4 class="review-title">Lorem Ipsum is simply dummy text of the printing and typesetting industry</h4>
                               <p class="review-des">Username ABC</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3 col-md-4 col-sm-6">
                       <div class="review-component">
                           <div class="review-image"> <img src="./assets/images/image-sample-review.jpeg" alt=""></div>
                           <div class="review-info">
                               <h4 class="review-title">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</h4>
                               <p class="review-des">Username ABC</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3 col-md-4 col-sm-6">
                       <div class="review-component">
                           <div class="review-image"> <img src="./assets/images/image-sample-review.jpeg" alt=""></div>
                           <div class="review-info">
                               <h4 class="review-title">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h4>
                               <p class="review-des">Username ABC</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3 col-md-4 col-sm-6">
                       <div class="review-component">
                           <div class="review-image"> <img src="./assets/images/image-sample-review.jpeg" alt=""></div>
                           <div class="review-info">
                               <h4 class="review-title">Lorem Ipsum is simply dummy text of the printing and typesetting industry</h4>
                               <p class="review-des">Username ABC</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3 col-md-4 col-sm-6">
                       <div class="review-component">
                           <div class="review-image"> <img src="./assets/images/image-sample-review.jpeg" alt=""></div>
                           <div class="review-info">
                               <h4 class="review-title">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</h4>
                               <p class="review-des">Username ABC</p>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3 col-md-4 col-sm-6">
                       <div class="review-component">
                           <div class="review-image"> <img src="./assets/images/image-sample-review.jpeg" alt=""></div>
                           <div class="review-info">
                               <h4 class="review-title">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h4>
                               <p class="review-des">Username ABC</p>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section>
   <div class="pagination-wrapper d-flex justify-content-center align-items-center flex-wrap">
       <div class="pagination-item arrow-left">&lt;</div>
       <div class="pagination-item active">1</div>
       <div class="pagination-item">2</div>
       <div class="pagination-item">3</div>
       <div class="pagination-item arrow-right">&gt;</div>
   </div>

@stop
