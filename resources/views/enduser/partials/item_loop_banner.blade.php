<div class="item">
    <a class="image-banner-item single" href="{{ $item->link }}">
        <img src="{{ $item->getImage() }}" alt="{{ $item->name }}">
    </a>
</div>
