@php
    $cate_slug = "chua-ro";
    if(isset($item->category->slug)){
        $cate_slug = $item->category->slug;
    }
    $link = route("course.courseDetail", [ 'slug_category' => $cate_slug, 'course_slug' => $item->slug ]);
@endphp
<div class="{{ $class }}">
    <div class="product-component">
        <div class="product-image"> <a href="{{ $link }}"><img src="{{ $item->getImage('standard') }}" alt=""></a>
{{--            <div class="product-label red">hot</div>--}}
            <div class="product-favorite"> <img src="{{asset("enduser/assets/icons/icon-heart.svg")}}" alt=""></div>
        </div><a class="product-title" href="{{ $link }}">{{$item->name}}</a>
        <div class="line"> </div>
        @if($item->price_base != $item->price_final)
            <div class="d-flex flex-wrap product-old-price">
                <del>{{number_format($item->price_base) }}đ</del>
            </div>
            <h5 class="product-price">{{number_format($item->price_final) }}đ</h5>
        @else
            <h5 class="product-price">{{number_format($item->price_base) }}đ</h5>
        @endif

        <div class="product-tags d-flex flex-wrap">
            <div class="tag-item">Đăng ký ngay</div>
        </div>
    </div>
</div>
