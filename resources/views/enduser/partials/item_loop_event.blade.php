<div class="{{ $class }}">
    <div class="new-box-component vertical"><a class="new-image" href="#">
            <img src="{{asset("enduser/assets/images/image-sample-news.jpg")}}" alt=""></a>
        <div class="new-info"> <a class="new-title" href="#">{{$item->name}}</a>
            <p></p>
            <p class="new-des countdownJs" data-date="{{$item->countdown}}"></p>
        </div>
    </div>
</div>
