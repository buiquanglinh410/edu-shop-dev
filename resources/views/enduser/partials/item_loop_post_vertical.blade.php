<div class="{{ $class }}"><a class="new-image" href="{{route('new.newDetail',['slug'=>$new->slug])}}"> <img src="{{$new->getImage()}}" alt="{{$new->name}}"></a>
    <div class="new-info"> <a class="new-title" href="{{route('new.newDetail',['slug'=>$new->slug])}}">{{$new->name}}</a>
        <p class="new-des">{{$new->description}} .</p>
    </div>
</div>
