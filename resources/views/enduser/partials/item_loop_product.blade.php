@php
    $link = $product->getLink();
@endphp

<div class="{{ $class }}">
    <div class="product-component">
        <div class="product-image"> <a href="{{ $link }}"><img src="{{$product->getImage()}}" alt=""></a>
            <div class="product-label {{$product->type == 'hot'? 'red':''}} ">{{$product->type == 'hot' ? "Hot" : ''}}</div>
            <div class="product-button-action d-flex align-items-center justify-content-center">
                <a class="btn" href="{{ $link }}">Mua hàng</a>
            </div>
        </div><a class="product-title" href="{{ $link }}">{{$product->name}}</a>
        <p>{{$product->short_description}}</p>
        <div class="line"> </div>
        <div class="d-flex flex-wrap product-old-price">
            <del>{{number_format($product->price_base) }}đ</del>
        </div>
        <h5 class="product-price">{{number_format($product->price_final) }}đ</h5>
        <div class="product-tags d-flex flex-wrap">
        </div>
    </div>
</div>
