<?php


/* === User === */
$prefix = "auth";
$controllerName = "auth";
Route::prefix($prefix)->name($controllerName . ".")->middleware("locale")->group(function() use($controllerName) {
    $controller = ucfirst($controllerName) . "Controller@";
    Route::get("/login", $controller . "login")->name("login");
    Route::post("/postLogin", $controller . "postLogin")->name("login.post");
    Route::get("/logout", $controller . "logout")->name("logout");
    Route::get("/change-lang/{lang}", $controller . "changeLang")->name("changeLang");
});
Route::namespace('Admin')->middleware("locale")->prefix('admin')->name('admin.')->group(function(){
    /* === User === */
    $prefix = "user";
    $controllerName = "user";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}/sendResetPassword", $controller . "sendResetPassword")->middleware('can:'.$controllerName.'.edit')->name("sendResetPassword");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === role === */
    $prefix = "role";
    $controllerName = "role";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === banner === */
    $prefix = "banner";
    $controllerName = "banner";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === discount === */
    $prefix = "discount";
    $controllerName = "discount";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === pages === */
    $prefix = "page";
    $controllerName = "page";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === blog tags === */
    $prefix = "blog_tags";
    $controllerName = "blog_tags";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === widgets === */
    $prefix = "widget";
    $controllerName = "widget";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === address === */
    $prefix = "address";
    $controllerName = "address";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === blog posts === */
    $prefix = "blog_posts";
    $controllerName = "blog_posts";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === product tags === */
    $prefix = "product tags";
    $controllerName = "product_tags";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === product products === */
    $prefix = "product_products";
    $controllerName = "product_products";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === product category === */
    $prefix = "product_category";
    $controllerName = "product_category";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === categories === */
    $prefix = "blog_categories";
    $controllerName = "blog_categories";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === contact === */
    $prefix = "contact";
    $controllerName = "contact";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });

    /* === Question === */
    $prefix = "qa_question";
    $controllerName = "qa_question";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });

     /* === Translate === */
     $prefix = "translate";
     $controllerName = "translate";
     Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
         $controller = ucfirst($controllerName) . "Controller@";
         Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
         Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
         Route::post("edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
     });
    /* === Event === */
    $prefix = "event";
    $controllerName = "event";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::post("edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === Answer === */
    $prefix = "qa_answer";
    $controllerName = "qa_answer";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === course_category === */
    $prefix = "course_category";
    $controllerName = "course_category";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === course === */
    $prefix = "course_courses";
    $controllerName = "course_courses";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");


    });
    /* === compo === */
    $prefix = "compo";
    $controllerName = "compo";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::post("/", $controller . "store")->name("store");
        Route::get("/delete/{id}", $controller . "delete")->name("delete");
        Route::post("/add-compo", $controller . "addToCompo")->name("addToCompo");
        Route::get("/delete-item-in-compo", $controller . "deleteItemInCompo")->name("deleteItemInCompo");

    });
    /* === lesson === */
    $prefix = "lesson";
    $controllerName = "lesson";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("edit/{lesson_id}", $controller . "editLesson")->name("editLesson");
        Route::post("upload-video", $controller . "uploadVideo")->name("uploadVideo");

    });
    /* === Notification === */
    $prefix = "notification";
    $controllerName = "notification";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === chapter === */
    $prefix = "chapter";
    $controllerName = "chapter";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/{course_id}", $controller . "index_chapter")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("{course_id}/create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("{course_id}/store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
        Route::post("ordering", $controller . "ordering")->middleware('can:' . $controllerName . '.edit')->name("ordering");
    });
    /* === lesson === */
    $prefix = "lesson";
    $controllerName = "lesson";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
    /* === Bank === */
    $prefix = "bank";
    $controllerName = "bank";
    Route::prefix($prefix)->name($controllerName . ".")->group(function() use($controllerName) {
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->middleware('can:'.$controllerName.'.index')->name("index");
        Route::get("create", $controller . "create")->middleware('can:'.$controllerName.'.create')->name("create");
        Route::post("store", $controller . "store")->middleware('can:'.$controllerName.'.create')->name("store");
        Route::get("{id}/edit", $controller . "edit")->middleware('can:'.$controllerName.'.edit')->name("edit");
        Route::post("{id}/edit", $controller . "update")->middleware('can:'.$controllerName.'.edit')->name("update");
        Route::get("{id}", $controller . "destroy")->middleware('can:'.$controllerName.'.destroy')->name("destroy");
        Route::post("multiDestroy", $controller . "multiDestroy")->middleware('can:'.$controllerName.'.destroy')->name("multiDestroy");
    });
});



?>
