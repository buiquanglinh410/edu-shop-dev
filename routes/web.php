<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require "admin.php";
Route::namespace('Enduser')->group(function(){
    /*------ End-user -------*/

    /*---- ajax ------*/
    Route::get('/ajax/get-district', 'HomeController@getDistrict')->name('ajax.getDistrict');
    Route::get('/ajax/get-ward', 'HomeController@getWard')->name('ajax.getWard');
    Route::get('/', 'HomeController@index')->name('home.index');
    /* --- AUTH --- */
    Route::get('/login', 'UserController@index')->name('user.login');
    Route::get('/login/post', 'UserController@postLogin')->name('user.postlogin');
    Route::get('/redirect/{driver}', 'UserController@redirectToProvider')->name('user.loginProvider');
    Route::get('/callback/{driver}', 'UserController@handleProviderCallback')->name('user.callbackSocial');
    Route::get('/register', 'UserController@register')->name('user.register');
    Route::post('/register/post', 'UserController@postRegister')->name('user.postregister');
    Route::get('/logout', 'UserController@logout')->name('user.logout');
    Route::get('/change-password', 'UserController@changePassword')->name('user.changePassword');
    Route::get('/forgot-password', 'UserController@forgotPassword')->name('user.forgotPassword');
    Route::post('/reset-password', 'UserController@sendMail')->name('user.senMail');
    Route::post('/reset-password/{token}', 'UserController@resetPassword')->name('user.resetPassword');



    /* -- ORDER -- */
    Route::get('/gio-hang', 'OrderController@cart')->name('order.cart');
    Route::get('them-vao-gio-hang', 'OrderController@addCart')->name('order.addCart');
    Route::get('xoa-gio-hang/{id}', 'OrderController@deleteCart')->name('order.deleteCart');
    Route::post('cart/update/{id}','OrderController@updateCart')->name('order.updateCart');

    Route::middleware('checklogin')->group(function(){
        Route::get('/thanh-toan', 'OrderController@checkout')->name('order.checkout');
        Route::post('/thanh-toan', 'OrderController@postCheckout')->name('order.postCheckout');
        Route::get('/thanh-toan-thanh-cong', 'OrderController@checkoutSuccess')->name('order.checkoutSuccess');

        /* --- ACCOUNT --- */
        Route::post('/change-profile', 'AccountController@changeProfile')->name('account.changeProfile');
        Route::post('/change-password', 'AccountController@changePassword')->name('account.changePassword');
        Route::get('/my-profile', 'AccountController@myProfile')->name('account.myProfile');
        Route::get('/my-coupon', 'AccountController@myCoupon')->name('account.myCoupon');
        Route::get('/my-courses', 'AccountController@myCourses')->name('account.myCourses');
        Route::get('/my-order', 'AccountController@myOrder')->name('account.myOrder');
        Route::get('/my-order-detail/{order_id}', 'AccountController@myOrderDetail')->name('account.myOrderDetail');
        Route::get('/address', 'AccountController@address')->name('account.address');
        Route::get('/add-address', 'AccountController@addAddress')->name('account.addAddress');
        Route::post('/post-become-teacher', 'AccountController@postBecomeTeacher')->name('account.postBecomeTeacher');
        Route::post('/khoa-hoc/add-comment', 'CourseController@addComment')->name('course.addComment');
        Route::post('/khoa-hoc/like-comment', 'CourseController@likeComment')->name('course.likeComment');
    });

    /* -- PRODUCT -- */
    Route::get('/product-lists', 'ProductController@productList')->name('product.productList');
    Route::get('/product-lists/{slug_category}', 'ProductController@productListByCategory')->name('product.productListByCategory');
    Route::get('/product-detail/{slug_category}/{slug}', 'ProductController@productDetail')->name('product.productDetail');
    /* -- COURSE -- */
   // Route::get('/course-detail', 'CourseController@courseDetail')->name('course.courseDetail');
    Route::get('/danh-sach-khoa-hoc', 'CourseController@courseList')->name('course.courseList');
    Route::get('/khoa-hoc/tag/{slug}', 'CourseController@courseTagSlug')->name('course.courseTagSlug');
    Route::get('/khoa-hoc/{slug_category}', 'CourseController@courseListInCategory')->name('course.courseListInCategory');
    Route::get('/khoa-hoc/{slug_category}/{course_slug}', 'CourseController@courseDetail')->name('course.courseDetail');
    Route::get('/lesson-detail', 'CourseController@lessionDetail')->name('course.lessionDetail');
    Route::get('/lesson-detail/{slug_lesson}', 'CourseController@lessionDetailChapter')->name('course.lessionDetailChapter');
    Route::get('/search-result', 'CourseController@search')->name('course.search');
    /* -- PAGE -- */
    Route::get('/become-teacher', 'PageController@becomeTeacher')->name('SiteTeacher');
    Route::get('/list-page', 'PageController@listPage')->name('page.listPage');
    Route::get('/notification-detail', 'PageController@notificationDetaill')->name('page.notificationDetaill');
    Route::get('/review', 'PageController@review')->name('page.review');
    Route::get('/tutorial', 'PageController@tutorial')->name('page.tutorial');
    Route::post('/question/post', 'PageController@postquestionClient')->name('page.questionClient');
    /* -- BLOG -- */
    Route::get('/tin-tuc', 'BlogController@newList')->name('new.newList');
//    Route::get('/tin-tuc/{slug_category}', 'BlogController@courseListInCategory')->name('course.courseListInCategory');
    Route::get('/tin-tuc/{slug}', 'BlogController@newDetail')->name('new.newDetail');
});



